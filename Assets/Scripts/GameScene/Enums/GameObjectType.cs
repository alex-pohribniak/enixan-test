﻿namespace GameScene.Enums
{
    public enum GameObjectType
    {
        Empty,
        Rock,
        Tree,
    }
}