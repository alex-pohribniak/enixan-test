﻿using System;
using GameScene.View;
using UnityEngine;

namespace GameScene.Enums
{
    public delegate void RockTypeGuidDelegate(RockType rockType, Guid value);

    public delegate void TreeTypeGuidDelegate(TreeType treeType, Guid value);

    public delegate void RockTypeDelegate(RockType rockType);

    public delegate void TreeTypeDelegate(TreeType treeType);

    public delegate void Vector3IntVector2IntDelegate(Vector3Int vector3Int, Vector2Int vector2Int);

    public delegate void RockViewDelegate(RockView rockView);

    public delegate void TreeViewDelegate(TreeView treeView);
}