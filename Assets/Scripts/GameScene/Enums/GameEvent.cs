﻿namespace GameScene.Enums
{
    public static class GameEvent
    {
        public const string GenerateRock = "GenerateRock";
        public const string GenerateRockComplete = "GenerateRockComplete";
        public const string GenerateTree = "GenerateTree";
        public const string GenerateTreeComplete = "GenerateTreeComplete";

        public const string GenerationIteration = "GenerationIteration";
        public const string PerimeterGenerationComplete = "PerimeterGenerationComplete";

        public const string AddRock = "AddRock";
        public const string AddTree = "AddTree";
        public const string AddObjectComplete = "AddObjectComplete";
        
        public const string ShowPossibleRock = "ShowPossibleRock";
        public const string ShowPossibleRockStart = "ShowPossibleRockStart";
        public const string ShowPossibleRockComplete = "ShowPossibleRockComplete";
        
        public const string ShowPossibleTree = "ShowPossibleTree";
        public const string ShowPossibleTreeStart = "ShowPossibleTreeStart";
        public const string ShowPossibleTreeComplete = "ShowPossibleTreeComplete";
        
        public const string MovePossibleObject = "MovePossibleObject";
        public const string MovePossibleObjectComplete = "MovePossibleObjectComplete";
        
        public const string CancelPossibleObject = "CancelPossibleObject";
        public const string AcceptPossibleObject = "AcceptPossibleObject";
        
        public const string VerifyPossibleRock = "VerifyPossibleRock";
        public const string VerifyPossibleTree = "VerifyPossibleTree";
        public const string VerifyFailed = "VerifyFailed";
        
        public const string GetRockInfo = "GetRockInfo";
        public const string GetTreeInfo = "GetTreeInfo";
    }
}