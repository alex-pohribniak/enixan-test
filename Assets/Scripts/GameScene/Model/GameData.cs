﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameScene.Config;
using GameScene.Enums;
using UnityEngine;

namespace GameScene.Model
{
    [Serializable]
    public class GameData
    {
        public static Vector3Int initialPositionForGeneration =
            new Vector3Int(GameMapConfig.MinXCoord, GameMapConfig.MaxYCoord, GameMapConfig.StartZCoord);

        public RockType rockForGeneration;
        public TreeType treeForGeneration;
        public bool isPerimeterGenerated;
        public Vector2Int availableObjectSize;
        public Vector3Int positionForGeneration = initialPositionForGeneration;
        public Vector3Int directionForGeneration = GameMapConfig.directionRightDown;
        public GameObjectData lastGeneratedObject;
        public Dictionary<Vector3Int, GameObjectData> gameObjectsData;
        public Dictionary<Vector2Int, List<GameObjectData>> sizeGameObjectRelation;

        public GameObjectData FindGameObjectDataByUniqueID(Guid uniqueID)
        {
            return (from gameObjectData in gameObjectsData
                where gameObjectData.Value.uniqueID == uniqueID
                select gameObjectData.Value).FirstOrDefault();
        }

        public List<Vector3Int> FindOccupiedTilesByUniqueID(Guid uniqueID)
        {
            return (from gameObjectData in gameObjectsData
                where gameObjectData.Value.uniqueID == uniqueID
                select gameObjectData.Key).ToList();
        }
    }
}