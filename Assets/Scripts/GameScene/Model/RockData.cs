﻿using System;
using System.Collections.Generic;
using Common.Singleton;
using GameScene.Config;
using GameScene.Enums;
using UnityEngine;

namespace GameScene.Model
{
    [Serializable]
    public class RockData : GameObjectData
    {
        public RockType rockType;

        public RockData(RockType rockType)
        {
            uniqueID = Guid.NewGuid();
            gameObjectType = GameObjectType.Rock;
            this.rockType = rockType;
            size = GetSizeByType(rockType);
        }

        public static Vector2Int GetSizeByType(RockType targetRockType)
        {
            return targetRockType switch
            {
                RockType.Zero => SizeConfig.size4X3,
                RockType.One => SizeConfig.size2X2,
                RockType.Two => SizeConfig.size3X3,
                RockType.Three => SizeConfig.size3X3,
                RockType.Four => SizeConfig.size3X3,
                RockType.Five => SizeConfig.size3X2,
                _ => throw new ArgumentOutOfRangeException()
            };
        }
        
        public static List<Vector3Int> DefineOccupiedTiles(RockType rockType)
        {
            var size = GetSizeByType(rockType);
            var occupiedTiles = new List<Vector3Int>();
            for (var x = 0; x < size.x; x++)
            for (var y = 0; y < size.y; y++)
            {
                var tilePosition = MainModel.GameData.positionForGeneration + new Vector3Int(
                    x * MainModel.GameData.directionForGeneration.x, y * MainModel.GameData.directionForGeneration.y,
                    GameMapConfig.DefaultZCoord);
                occupiedTiles.Add(tilePosition);
            }

            return occupiedTiles;
        }
    }
}