﻿using System;
using System.Collections.Generic;
using Common.Singleton;
using GameScene.Config;
using GameScene.Enums;
using UnityEngine;

namespace GameScene.Model
{
    [Serializable]
    public class TreeData : GameObjectData
    {
        public TreeType treeType;

        public TreeData(TreeType treeType)
        {
            uniqueID = Guid.NewGuid();
            gameObjectType = GameObjectType.Tree;
            this.treeType = treeType;
            size = GetSizeByType(treeType);
        }

        public static Vector2Int GetSizeByType(TreeType targetTreeType)
        {
            return targetTreeType switch
            {
                TreeType.Zero => SizeConfig.size6X6,
                TreeType.One => SizeConfig.size1X1,
                TreeType.Two => SizeConfig.size2X2,
                TreeType.Three => SizeConfig.size2X2,
                TreeType.Four => SizeConfig.size2X2,
                TreeType.Five => SizeConfig.size2X2,
                TreeType.Six => SizeConfig.size2X2,
                TreeType.Seven => SizeConfig.size3X3,
                _ => throw new ArgumentOutOfRangeException()
            };
        }
        
        public static List<Vector3Int> DefineOccupiedTiles(TreeType treeType)
        {
            var size = GetSizeByType(treeType);
            var occupiedTiles = new List<Vector3Int>();
            for (var x = 0; x < size.x; x++)
            for (var y = 0; y < size.y; y++)
            {
                var tilePosition = MainModel.GameData.positionForGeneration + new Vector3Int(
                    x * MainModel.GameData.directionForGeneration.x, y * MainModel.GameData.directionForGeneration.y,
                    GameMapConfig.DefaultZCoord);
                occupiedTiles.Add(tilePosition);
            }

            return occupiedTiles;
        }
    }
}