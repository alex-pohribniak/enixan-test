﻿using System;
using GameScene.Enums;
using UnityEngine;

namespace GameScene.Model
{
    [Serializable]
    public class GameObjectData
    {
        public Guid uniqueID;
        public GameObjectType gameObjectType;
        public Vector2Int size;

        public GameObjectData()
        {
        }

        public GameObjectData(GameObjectType gameObjectType)
        {
            this.gameObjectType = gameObjectType;
            uniqueID = Guid.Empty;
            size = Vector2Int.one;
        }
    }
}