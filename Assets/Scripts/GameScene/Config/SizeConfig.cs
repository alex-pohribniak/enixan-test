﻿using UnityEngine;

namespace GameScene.Config
{
    public static class SizeConfig
    {
        public static Vector2Int size1X1 = new Vector2Int(1,1);
        public static Vector2Int size2X2 = new Vector2Int(2,2);
        public static Vector2Int size3X2 = new Vector2Int(3,2);
        public static Vector2Int size3X3 = new Vector2Int(3,3);
        public static Vector2Int size4X3 = new Vector2Int(4,3);
        public static Vector2Int size6X6 = new Vector2Int(6,6);
    }
}