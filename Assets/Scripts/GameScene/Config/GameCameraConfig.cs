﻿using UnityEngine;

namespace GameScene.Config
{
    public static class GameCameraConfig
    {
        public static Vector3 startPosition = new Vector3(0, 1250f, -2250);
        public static Vector3 startRotation = new Vector3(30f, 0, 0);

        public const int MinXCoord = -1250;
        public const int MaxXCoord = 1250;
        // public const int MinYCoord = 1;
        // public const int MaxYCoord = 1250;
        public const int MinZCoord = -2250;
        public const int MaxZCoord = 250;

        public const int MoveXMultiplier = 20;
        public const int MoveZMultiplier = 30;
        
        
        public const float ZoomStep = 1f;
        public const float NearZoomLimit = 10;
        public const float FarZoomLimit = 45;
        
        
    }
}