﻿using UnityEngine;

namespace GameScene.Config
{
    public static class GameMapConfig
    {
        public const int StartXCoord = -20;
        public const int StartYCoord = 20;
        public const int StartZCoord = 0;

        public const int MinXCoord = -20;
        public const int MaxXCoord = 20;

        public const int MinYCoord = -20;
        public const int MaxYCoord = 20;

        public const int DefaultZCoord = 0;

        public static Vector2Int tileSize = Vector2Int.one;
        
        public static Vector3Int directionRightDown = new Vector3Int(1, -1, 0);
        public static Vector3Int directionLeftDown = new Vector3Int(-1, -1, 0);
        public static Vector3Int directionLeftUp = new Vector3Int(-1, 1, 0);
        public static Vector3Int directionRightUp = new Vector3Int(1, 1, 0);
    }
}