﻿namespace GameScene.Config
{
    public static class GameObjectConfig
    {
        public const int MaxXSize = 6;
        public const int MaxYSize = 6;
    }
}