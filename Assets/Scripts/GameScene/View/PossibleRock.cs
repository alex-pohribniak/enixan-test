﻿using System.Linq;
using Common.Enums;
using Common.Singleton;
using GameScene.Enums;
using GameScene.Model;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace GameScene.View
{
    public class PossibleRock : MonoBehaviour
    {
        public Tilemap grassTileMap;
        public GameObject possibleGeneratedRock;
        private readonly SimpleDelegate _onAcceptPossibleObject;
        private readonly SimpleDelegate _onAddObjectComplete;
        private readonly SimpleDelegate _onCancelPossibleObject;
        private readonly SimpleDelegate _onMovePossibleObjectComplete;
        private readonly SimpleDelegate _onShowPossibleRockStart;

        public PossibleRock()
        {
            _onAddObjectComplete = OnAddObjectComplete;
            _onAcceptPossibleObject = OnAcceptPossibleObject;
            _onCancelPossibleObject = OnCancelPossibleObject;
            _onMovePossibleObjectComplete = OnMovePossibleObjectComplete;
            _onShowPossibleRockStart = OnShowPossibleRockStart;
        }

        private void Awake()
        {
            Observer.AddListener(GameEvent.ShowPossibleRockStart, _onShowPossibleRockStart);
            Observer.AddListener(GameEvent.MovePossibleObjectComplete, _onMovePossibleObjectComplete);
            Observer.AddListener(GameEvent.CancelPossibleObject, _onCancelPossibleObject);
            Observer.AddListener(GameEvent.AcceptPossibleObject, _onAcceptPossibleObject);
            Observer.AddListener(GameEvent.AddObjectComplete, _onAddObjectComplete);
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(GameEvent.ShowPossibleRockStart, _onShowPossibleRockStart);
            Observer.RemoveListener(GameEvent.MovePossibleObjectComplete, _onMovePossibleObjectComplete);
            Observer.RemoveListener(GameEvent.CancelPossibleObject, _onCancelPossibleObject);
            Observer.RemoveListener(GameEvent.AcceptPossibleObject, _onAcceptPossibleObject);
            Observer.RemoveListener(GameEvent.AddObjectComplete, _onAddObjectComplete);
        }

        private void OnAddObjectComplete()
        {
            DestroyRock();
        }

        private void OnAcceptPossibleObject()
        {
            if (possibleGeneratedRock != null) Observer.Emit(GameEvent.VerifyPossibleRock);
        }

        private void OnCancelPossibleObject()
        {
            DestroyRock();
        }

        private void DestroyRock()
        {
            if (possibleGeneratedRock == null) return;
            RocksObjectPools.PutBackPooledObject(possibleGeneratedRock);
            possibleGeneratedRock = null;
        }

        private void OnMovePossibleObjectComplete()
        {
            if (possibleGeneratedRock == null || possibleGeneratedRock.activeInHierarchy == false) return;
            PositionRock();
        }

        private void OnShowPossibleRockStart()
        {
            possibleGeneratedRock =
                RocksObjectPools.sharedInstance.GetPooledObject(MainModel.GameData.rockForGeneration);
            PositionRock();
            possibleGeneratedRock.SetActive(true);
            Observer.Emit(GameEvent.ShowPossibleRockComplete);
        }

        private void PositionRock()
        {
            var possibleOccupiedTiles = RockData.DefineOccupiedTiles(MainModel.GameData.rockForGeneration);
            var possibleOccupiedTilesCenterPositions = possibleOccupiedTiles
                .Select(occupiedTile => grassTileMap.GetCellCenterWorld(occupiedTile)).ToList();

            var bounds = new Bounds(possibleOccupiedTilesCenterPositions.FirstOrDefault(), Vector3.zero);
            for (var i = 1; i < possibleOccupiedTilesCenterPositions.Count; i++)
                bounds.Encapsulate(possibleOccupiedTilesCenterPositions[i]);
            var possibleRockPosition = bounds.center;

            possibleGeneratedRock.transform.position = possibleRockPosition;
        }
    }
}