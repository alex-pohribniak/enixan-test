﻿using System.Collections.Generic;
using System.Linq;
using GameScene.Enums;
using UnityEngine;

namespace GameScene.View
{
    public class TreesObjectPools : MonoBehaviour
    {
        public static TreesObjectPools sharedInstance;

        public List<TreeObjectPool> treeObjectPools;

        private void Awake()
        {
            sharedInstance = this;
        }

        private void Start()
        {
            foreach (var treeObjectPool in treeObjectPools)
            {
                treeObjectPool.pooledObjects = new List<GameObject>();

                for (var i = 0; i < treeObjectPool.amountToPool; i++)
                {
                    var tmp = Instantiate(treeObjectPool.objectToPool, treeObjectPool.parentObject);
                    tmp.SetActive(false);
                    treeObjectPool.pooledObjects.Add(tmp);
                }
            }
        }

        public GameObject GetPooledObject(TreeType treeType)
        {
            foreach (var treeObjectPool in treeObjectPools.Where(treeObjectPool => treeObjectPool.treeType == treeType))
                for (var i = 0; i < treeObjectPool.amountToPool; i++)
                    if (!treeObjectPool.pooledObjects[i].activeInHierarchy)
                        return treeObjectPool.pooledObjects[i];

            return null;
        }
        
        public static void PutBackPooledObject(GameObject targetGameObject)
        {
            targetGameObject.SetActive(false);
        }
    }
}