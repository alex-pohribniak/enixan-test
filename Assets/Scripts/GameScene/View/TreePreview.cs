﻿using Common.Singleton;
using GameScene.Enums;
using UnityEngine;

namespace GameScene.View
{
    public class TreePreview : MonoBehaviour
    {
        public TreeType treeType;

        public void AddTree()
        {
            Observer.Emit(GameEvent.ShowPossibleTree, treeType);
        }
    }
}