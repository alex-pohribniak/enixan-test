﻿using System.Collections.Generic;
using UnityEngine;

namespace GameScene.View
{
    public class ObjectPool : MonoBehaviour
    {
        public Transform parentObject;
        public GameObject objectToPool;
        public int amountToPool;
        public List<GameObject> pooledObjects;
    }
}