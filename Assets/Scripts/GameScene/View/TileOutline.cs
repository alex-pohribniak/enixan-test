﻿using UnityEngine;

namespace GameScene.View
{
    public class TileOutline : MonoBehaviour
    {
        public GameObject tileOutline;

        public void ChangeVision()
        {
            tileOutline.gameObject.SetActive(!tileOutline.activeSelf);
        }
    }
}