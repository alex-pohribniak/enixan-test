﻿using System;
using Common.Singleton;
using GameScene.Config;
using GameScene.Enums;
using UnityEngine;

namespace GameScene.View
{
    public class CameraController : MonoBehaviour
    {
        public Camera gameCamera;

        public Vector2 touchStartPosition;
        public Vector2 touchDirection;

        private void Start()
        {
            var cameraTransform = gameCamera.transform;
            cameraTransform.position = GameCameraConfig.startPosition;
            cameraTransform.eulerAngles = GameCameraConfig.startRotation;
            touchStartPosition = GameCameraConfig.startPosition;
        }

        private void Update()
        {
            switch (Input.touchCount)
            {
                case 1:
                {
                    var touch = Input.GetTouch(0);
                    switch (touch.phase)
                    {
                        case TouchPhase.Began:
                            touchStartPosition = touch.position;
                            break;
                        case TouchPhase.Moved:
                            // touchDirection = touch.position - touchStartPosition;
                            var touchDirectionX = touch.position.x - touchStartPosition.x;
                            var touchDirectionY = touch.position.y - touchStartPosition.y;
                            if (gameCamera.transform.position.x - touchDirectionX > GameCameraConfig.MinXCoord &&
                                gameCamera.transform.position.x - touchDirectionX < GameCameraConfig.MaxXCoord)
                                touchDirection.x = touchDirectionX;
                            else
                                touchDirection.x = 0f;

                            if (gameCamera.transform.position.z - touchDirectionY > GameCameraConfig.MinZCoord &&
                                gameCamera.transform.position.z - touchDirectionY < GameCameraConfig.MaxZCoord)
                                touchDirection.y = touchDirectionY;
                            else
                                touchDirection.y = 0f;
                            var normalizedShift = new Vector3(touchDirection.x, 0, touchDirection.y).normalized;
                            normalizedShift.x *= GameCameraConfig.MoveXMultiplier;
                            normalizedShift.z *= GameCameraConfig.MoveZMultiplier;
                            gameCamera.transform.position -= normalizedShift;
                            break;
                        case TouchPhase.Stationary:
                            break;
                        case TouchPhase.Ended:
                            CheckObjectInfo(touch.position);
                            break;
                        case TouchPhase.Canceled:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    break;
                }
                case 2:
                {
                    var touch0 = Input.GetTouch(0);
                    var touch1 = Input.GetTouch(1);
                    var touch0PreviousPosition = touch0.position - touch0.deltaPosition;
                    var touch1PreviousPosition = touch1.position - touch1.deltaPosition;
                    var previousDistanceBetweenTouches =
                        Vector2.Distance(touch1PreviousPosition, touch0PreviousPosition);
                    var currentDistanceBetweenTouches = Vector2.Distance(touch1.position, touch0.position);

                    if (gameCamera.fieldOfView > GameCameraConfig.NearZoomLimit)
                        if (currentDistanceBetweenTouches > previousDistanceBetweenTouches)
                            gameCamera.fieldOfView -= GameCameraConfig.ZoomStep;

                    if (gameCamera.fieldOfView < GameCameraConfig.FarZoomLimit)
                        if (previousDistanceBetweenTouches > currentDistanceBetweenTouches)
                            gameCamera.fieldOfView += GameCameraConfig.ZoomStep;

                    break;
                }
            }
        }

        private void CheckObjectInfo(Vector3 position)
        {
            var ray = gameCamera.ScreenPointToRay(position);
            if (!Physics.Raycast(ray, out var hitInfo)) return;
            if (hitInfo.collider.gameObject.GetComponent<RockView>() != null)
            {
                var rockView = hitInfo.collider.gameObject.GetComponent<RockView>();
                Observer.Emit(GameEvent.GetRockInfo, rockView);
            }
            else if (hitInfo.collider.gameObject.GetComponent<TreeView>() != null)
            {
                var treeView = hitInfo.collider.gameObject.GetComponent<TreeView>();
                Observer.Emit(GameEvent.GetTreeInfo, treeView);
            }
        }
    }
}