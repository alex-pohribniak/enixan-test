﻿using System.Collections.Generic;
using System.Linq;
using GameScene.Enums;
using UnityEngine;

namespace GameScene.View
{
    public class RocksObjectPools : MonoBehaviour
    {
        public static RocksObjectPools sharedInstance;

        public List<RockObjectPool> rockObjectPools;

        private void Awake()
        {
            sharedInstance = this;
        }

        private void Start()
        {
            foreach (var rockObjectPool in rockObjectPools)
            {
                rockObjectPool.pooledObjects = new List<GameObject>();

                for (var i = 0; i < rockObjectPool.amountToPool; i++)
                {
                    var tmp = Instantiate(rockObjectPool.objectToPool, rockObjectPool.parentObject);
                    tmp.SetActive(false);
                    rockObjectPool.pooledObjects.Add(tmp);
                }
            }
        }

        public GameObject GetPooledObject(RockType rockType)
        {
            foreach (var rockObjectPool in rockObjectPools.Where(rockObjectPool => rockObjectPool.rockType == rockType))
                for (var i = 0; i < rockObjectPool.amountToPool; i++)
                    if (!rockObjectPool.pooledObjects[i].activeInHierarchy)
                        return rockObjectPool.pooledObjects[i];

            return null;
        }

        public static void PutBackPooledObject(GameObject targetGameObject)
        {
            targetGameObject.SetActive(false);
        }
    }
}