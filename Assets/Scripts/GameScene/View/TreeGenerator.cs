﻿using System;
using System.Linq;
using Common.Singleton;
using Common.View;
using GameScene.Enums;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace GameScene.View
{
    public class TreeGenerator : MonoBehaviour
    {
        public Tilemap grassTileMap;
        private readonly TreeTypeGuidDelegate _onGenerateTree;

        public TreeGenerator()
        {
            _onGenerateTree = OnGenerateTree;
        }

        private void Awake()
        {
            Observer.AddListener(GameEvent.GenerateTree, _onGenerateTree);
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(GameEvent.GenerateTree, _onGenerateTree);
        }

        private void OnGenerateTree(TreeType treeType, Guid uniqueID)
        {
            var gameObjectData = MainModel.GameData.FindGameObjectDataByUniqueID(uniqueID);
            var occupiedTiles = MainModel.GameData.FindOccupiedTilesByUniqueID(uniqueID);
            var occupiedTilesCenterPositions =
                occupiedTiles.Select(occupiedTile => grassTileMap.GetCellCenterWorld(occupiedTile)).ToList();

            var bounds = new Bounds(occupiedTilesCenterPositions.FirstOrDefault(), Vector3.zero);
            for (var i = 1; i < occupiedTilesCenterPositions.Count; i++)
                bounds.Encapsulate(occupiedTilesCenterPositions[i]);
            var treePosition = bounds.center;


            var generatedTree = TreesObjectPools.sharedInstance.GetPooledObject(treeType);
            generatedTree.transform.position = treePosition;
            generatedTree.SetActive(true);

            var treeView = generatedTree.GetComponent<TreeView>();
            treeView.uniqueID = uniqueID;
            treeView.customName = generatedTree.name;
            treeView.size = $"{gameObjectData.size.x} x {gameObjectData.size.y}";

            Observer.Emit(GameEvent.GenerateTreeComplete);
        }
    }
}