﻿using Common.Singleton;
using GameScene.Enums;
using UnityEngine;

namespace GameScene.View
{
    public class RockPreview : MonoBehaviour
    {
        public RockType rockType;

        public void AddRock()
        {
            Observer.Emit(GameEvent.ShowPossibleRock, rockType);
        }
    }
}