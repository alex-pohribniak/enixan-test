﻿using System;
using UnityEngine;

namespace GameScene.View
{
    public class TreeView : MonoBehaviour
    {
        public Guid uniqueID;
        public string customName;
        public string size;
    }
}