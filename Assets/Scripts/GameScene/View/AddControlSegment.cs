using System;
using System.Collections.Generic;
using Common.Enums;
using Common.Singleton;
using GameScene.Enums;
using UnityEngine;
using UnityEngine.UI;

namespace GameScene.View
{
    public class AddControlSegment : MonoBehaviour
    {
        public List<GameObject> gameObjectsForActivationSwap;
        public GameObject cameraControl;
        public Text infoText;
        public GameObject shopButton;
        private readonly SimpleDelegate _onAddObjectComplete;
        private readonly RockViewDelegate _onGetRockInfo;
        private readonly TreeViewDelegate _onGetTreeInfo;
        private readonly RockTypeDelegate _onShowPossibleRock;
        private readonly TreeTypeDelegate _onShowPossibleTree;
        private readonly SimpleDelegate _onVerifyFailed;
        private readonly SimpleDelegate _onPerimeterGenerationComplete;


        public AddControlSegment()
        {
            _onPerimeterGenerationComplete = OnPerimeterGenerationComplete;
            _onGetTreeInfo = OnGetTreeInfo;
            _onGetRockInfo = OnGetRockInfo;
            _onVerifyFailed = OnVerifyFailed;
            _onShowPossibleRock = OnShowPossibleRock;
            _onShowPossibleTree = OnShowPossibleTree;
            _onAddObjectComplete = OnAddObjectComplete;
        }

        private void OnPerimeterGenerationComplete()
        {
            shopButton.SetActive(true);
        }

        private void Awake()
        {
            Observer.AddListener(GameEvent.ShowPossibleRock, _onShowPossibleRock);
            Observer.AddListener(GameEvent.ShowPossibleTree, _onShowPossibleTree);
            Observer.AddListener(GameEvent.AddObjectComplete, _onAddObjectComplete);
            Observer.AddListener(GameEvent.VerifyFailed, _onVerifyFailed);
            Observer.AddListener(GameEvent.GetRockInfo, _onGetRockInfo);
            Observer.AddListener(GameEvent.GetTreeInfo, _onGetTreeInfo);
            Observer.AddListener(GameEvent.PerimeterGenerationComplete, _onPerimeterGenerationComplete);
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(GameEvent.ShowPossibleRock, _onShowPossibleRock);
            Observer.RemoveListener(GameEvent.ShowPossibleTree, _onShowPossibleTree);
            Observer.RemoveListener(GameEvent.AddObjectComplete, _onAddObjectComplete);
            Observer.RemoveListener(GameEvent.VerifyFailed, _onVerifyFailed);
            Observer.RemoveListener(GameEvent.GetRockInfo, _onGetRockInfo);
            Observer.RemoveListener(GameEvent.GetTreeInfo, _onGetTreeInfo);
            Observer.RemoveListener(GameEvent.PerimeterGenerationComplete, _onPerimeterGenerationComplete);
        }

        private void OnGetTreeInfo(TreeView treeView)
        {
            infoText.text = $"uuid = {treeView.uniqueID}\nname = {treeView.customName}\nsize = {treeView.size}";
            Debug.Log($"uuid = {treeView.uniqueID}\nname = {treeView.customName}\nsize = {treeView.size}");
        }

        private void OnGetRockInfo(RockView rockView)
        {
            infoText.text = $"uuid = {rockView.uniqueID}\nname = {rockView.customName}\nsize = {rockView.size}";
            Debug.Log($"uuid = {rockView.uniqueID}\nname = {rockView.customName}\nsize = {rockView.size}");
        }

        private void OnVerifyFailed()
        {
            infoText.text = "Objects intersection happened";
        }

        private void ResetInfoText()
        {
            infoText.text = "";
        }

        private void OnAddObjectComplete()
        {
            ResetInfoText();
            ActivationSwap();
            cameraControl.SetActive(true);
            infoText.gameObject.SetActive(true);
        }

        private void OnShowPossibleTree(TreeType treeType)
        {
            ResetInfoText();
            ActivationSwap();
        }

        private void OnShowPossibleRock(RockType rockType)
        {
            ResetInfoText();
            ActivationSwap();
        }

        public void Accept()
        {
            Observer.Emit(GameEvent.AcceptPossibleObject);
        }

        public void Cancel()
        {
            ResetInfoText();
            ActivationSwap();
            cameraControl.SetActive(true);
            Observer.Emit(GameEvent.CancelPossibleObject);
        }

        private void ActivationSwap()
        {
            foreach (var targetObject in gameObjectsForActivationSwap) targetObject.SetActive(!targetObject.activeSelf);
        }
    }
}