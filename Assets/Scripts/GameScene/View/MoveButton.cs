﻿using Common.Enums;
using Common.Singleton;
using GameScene.Enums;
using GameScene.Model;
using UnityEngine;

namespace GameScene.View
{
    public class MoveButton : MonoBehaviour
    {
        public Vector3Int moveDirection;
        public Vector2Int possibleObjectSize;
        private readonly SimpleDelegate _onShowPossibleRockComplete;
        private readonly SimpleDelegate _onShowPossibleTreeComplete;

        public MoveButton()
        {
            _onShowPossibleRockComplete = OnShowPossibleRockComplete;
            _onShowPossibleTreeComplete = OnShowPossibleTreeComplete;
        }

        private void Awake()
        {
            Observer.AddListener(GameEvent.ShowPossibleRockComplete, _onShowPossibleRockComplete);
            Observer.AddListener(GameEvent.ShowPossibleTreeComplete, _onShowPossibleTreeComplete);
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(GameEvent.ShowPossibleRockComplete, _onShowPossibleRockComplete);
            Observer.RemoveListener(GameEvent.ShowPossibleTreeComplete, _onShowPossibleTreeComplete);
        }

        private void OnShowPossibleTreeComplete()
        {
            possibleObjectSize = TreeData.GetSizeByType(MainModel.GameData.treeForGeneration);
        }

        private void OnShowPossibleRockComplete()
        {
            possibleObjectSize = RockData.GetSizeByType(MainModel.GameData.rockForGeneration);
        }

        public void Move()
        {
            Observer.Emit(GameEvent.MovePossibleObject, moveDirection, possibleObjectSize);
        }
    }
}