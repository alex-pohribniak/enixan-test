﻿using System.Linq;
using Common.Enums;
using Common.Singleton;
using GameScene.Enums;
using GameScene.Model;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace GameScene.View
{
    public class PossibleTree : MonoBehaviour
    {
        public Tilemap grassTileMap;
        public GameObject possibleGeneratedTree;
        private readonly SimpleDelegate _onAcceptPossibleObject;
        private readonly SimpleDelegate _onAddObjectComplete;
        private readonly SimpleDelegate _onCancelPossibleObject;
        private readonly SimpleDelegate _onMovePossibleObjectComplete;
        private readonly SimpleDelegate _onShowPossibleTreeStart;

        public PossibleTree()
        {
            _onAddObjectComplete = OnAddObjectComplete;
            _onAcceptPossibleObject = OnAcceptPossibleObject;
            _onCancelPossibleObject = OnCancelPossibleObject;
            _onMovePossibleObjectComplete = OnMovePossibleObjectComplete;
            _onShowPossibleTreeStart = OnShowPossibleTreeStart;
        }

        private void Awake()
        {
            Observer.AddListener(GameEvent.ShowPossibleTreeStart, _onShowPossibleTreeStart);
            Observer.AddListener(GameEvent.MovePossibleObjectComplete, _onMovePossibleObjectComplete);
            Observer.AddListener(GameEvent.CancelPossibleObject, _onCancelPossibleObject);
            Observer.AddListener(GameEvent.AcceptPossibleObject, _onAcceptPossibleObject);
            Observer.AddListener(GameEvent.AddObjectComplete, _onAddObjectComplete);
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(GameEvent.ShowPossibleTreeStart, _onShowPossibleTreeStart);
            Observer.RemoveListener(GameEvent.MovePossibleObjectComplete, _onMovePossibleObjectComplete);
            Observer.RemoveListener(GameEvent.CancelPossibleObject, _onCancelPossibleObject);
            Observer.RemoveListener(GameEvent.AcceptPossibleObject, _onAcceptPossibleObject);
            Observer.RemoveListener(GameEvent.AddObjectComplete, _onAddObjectComplete);
        }

        private void OnAcceptPossibleObject()
        {
            if (possibleGeneratedTree != null) Observer.Emit(GameEvent.VerifyPossibleTree);
        }

        private void OnAddObjectComplete()
        {
            DestroyTree();
        }

        private void DestroyTree()
        {
            if (possibleGeneratedTree == null) return;
            TreesObjectPools.PutBackPooledObject(possibleGeneratedTree);
            possibleGeneratedTree = null;
        }

        private void OnCancelPossibleObject()
        {
            DestroyTree();
        }

        private void OnMovePossibleObjectComplete()
        {
            if (possibleGeneratedTree == null || possibleGeneratedTree.activeInHierarchy == false) return;
            PositionTree();
        }

        private void OnShowPossibleTreeStart()
        {
            possibleGeneratedTree =
                TreesObjectPools.sharedInstance.GetPooledObject(MainModel.GameData.treeForGeneration);
            PositionTree();
            possibleGeneratedTree.SetActive(true);
            Observer.Emit(GameEvent.ShowPossibleTreeComplete);
        }

        private void PositionTree()
        {
            var possibleOccupiedTiles = TreeData.DefineOccupiedTiles(MainModel.GameData.treeForGeneration);
            var possibleOccupiedTilesCenterPositions = possibleOccupiedTiles
                .Select(occupiedTile => grassTileMap.GetCellCenterWorld(occupiedTile)).ToList();

            var bounds = new Bounds(possibleOccupiedTilesCenterPositions.FirstOrDefault(), Vector3.zero);
            for (var i = 1; i < possibleOccupiedTilesCenterPositions.Count; i++)
                bounds.Encapsulate(possibleOccupiedTilesCenterPositions[i]);
            var possibleTreePosition = bounds.center;

            possibleGeneratedTree.transform.position = possibleTreePosition;
        }
    }
}