﻿using System;
using UnityEngine;

namespace GameScene.View
{
    public class RockView : MonoBehaviour
    {
        public Guid uniqueID;
        public string customName;
        public string size;
    }
}