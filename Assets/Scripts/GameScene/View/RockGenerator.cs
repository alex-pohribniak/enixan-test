﻿using System;
using System.Linq;
using Common.Singleton;
using GameScene.Enums;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace GameScene.View
{
    public class RockGenerator : MonoBehaviour
    {
        public Tilemap grassTileMap;
        private readonly RockTypeGuidDelegate _onGenerateRock;

        public RockGenerator()
        {
            _onGenerateRock = OnGenerateRock;
        }

        private void Awake()
        {
            Observer.AddListener(GameEvent.GenerateRock, _onGenerateRock);
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(GameEvent.GenerateRock, _onGenerateRock);
        }

        private void OnGenerateRock(RockType rockType, Guid uniqueID)
        {
            var gameObjectData = MainModel.GameData.FindGameObjectDataByUniqueID(uniqueID);
            var occupiedTiles = MainModel.GameData.FindOccupiedTilesByUniqueID(uniqueID);
            var occupiedTilesCenterPositions =
                occupiedTiles.Select(occupiedTile => grassTileMap.GetCellCenterWorld(occupiedTile)).ToList();

            var bounds = new Bounds(occupiedTilesCenterPositions.FirstOrDefault(), Vector3.zero);
            for (var i = 1; i < occupiedTilesCenterPositions.Count; i++)
                bounds.Encapsulate(occupiedTilesCenterPositions[i]);
            var rockPosition = bounds.center;

            var generatedRock = RocksObjectPools.sharedInstance.GetPooledObject(rockType);
            generatedRock.transform.position = rockPosition;
            generatedRock.SetActive(true);

            var rockView = generatedRock.GetComponent<RockView>();
            rockView.uniqueID = uniqueID;
            rockView.customName = generatedRock.name;
            rockView.size = $"{gameObjectData.size.x} x {gameObjectData.size.y}";

            Observer.Emit(GameEvent.GenerateRockComplete);
        }
    }
}