﻿using Common.Singleton;
using Common.Tasks;
using GameScene.Config;
using UnityEngine;
using UnityEngine.Android;

namespace GameScene.Logic
{
    public class DefineDirectionForGeneration : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();

            
            foreach (var gameObjectData in MainModel.GameData.gameObjectsData)
                if (gameObjectData.Value == MainModel.GameData.lastGeneratedObject)
                {
                    if (gameObjectData.Key == new Vector3Int(GameMapConfig.MinXCoord, GameMapConfig.MaxYCoord,
                            GameMapConfig.DefaultZCoord))
                        MainModel.GameData.directionForGeneration = GameMapConfig.directionRightDown;
                    else if (gameObjectData.Key == new Vector3Int(GameMapConfig.MaxXCoord, GameMapConfig.MaxYCoord,
                            GameMapConfig.DefaultZCoord))
                        MainModel.GameData.directionForGeneration = GameMapConfig.directionLeftDown;
                    else if (gameObjectData.Key == new Vector3Int(GameMapConfig.MaxXCoord, GameMapConfig.MinYCoord,
                            GameMapConfig.DefaultZCoord))
                        MainModel.GameData.directionForGeneration = GameMapConfig.directionLeftUp;
                    else if (gameObjectData.Key == new Vector3Int(GameMapConfig.MinXCoord, GameMapConfig.MinYCoord,
                            GameMapConfig.DefaultZCoord))
                        MainModel.GameData.directionForGeneration = GameMapConfig.directionRightUp;
                }
            
            Complete();
        }

        protected override bool Guard()
        {
            return MainModel.GameData.isPerimeterGenerated != true;
        }
    }
}