using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using GameScene.Config;
using GameScene.Enums;
using UnityEngine;

namespace GameScene.Logic
{
    public class ShowPossibleTree : BaseTask
    {
        private readonly SimpleDelegate _onShowPossibleTreeComplete;

        public ShowPossibleTree()
        {
            _onShowPossibleTreeComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            MainModel.GameData.directionForGeneration = GameMapConfig.directionRightDown;
            MainModel.GameData.positionForGeneration = Vector3Int.zero;
            Observer.Emit(GameEvent.ShowPossibleTreeStart);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(GameEvent.ShowPossibleTreeComplete, _onShowPossibleTreeComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(GameEvent.ShowPossibleTreeComplete, _onShowPossibleTreeComplete);
        }
    }
}