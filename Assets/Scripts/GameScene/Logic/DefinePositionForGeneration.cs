﻿using Common.Singleton;
using Common.Tasks;
using GameScene.Config;
using UnityEngine;

namespace GameScene.Logic
{
    public class DefinePositionForGeneration : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();


            if (MainModel.GameData.directionForGeneration == GameMapConfig.directionRightDown)
                MainModel.GameData.positionForGeneration =
                    new Vector3Int(
                        MainModel.GameData.positionForGeneration.x + MainModel.GameData.lastGeneratedObject.size.x,
                        GameMapConfig.MaxYCoord, 0);
            else if (MainModel.GameData.directionForGeneration == GameMapConfig.directionLeftDown)
                MainModel.GameData.positionForGeneration =
                    new Vector3Int(GameMapConfig.MaxXCoord,
                        MainModel.GameData.positionForGeneration.y - MainModel.GameData.lastGeneratedObject.size.y, 0);
            else if (MainModel.GameData.directionForGeneration == GameMapConfig.directionLeftUp)
                MainModel.GameData.positionForGeneration =
                    new Vector3Int(
                        MainModel.GameData.positionForGeneration.x - MainModel.GameData.lastGeneratedObject.size.x,
                        GameMapConfig.MinYCoord, 0);
            else if (MainModel.GameData.directionForGeneration == GameMapConfig.directionRightUp)
                MainModel.GameData.positionForGeneration =
                    new Vector3Int(GameMapConfig.MinXCoord,
                        MainModel.GameData.positionForGeneration.y + MainModel.GameData.lastGeneratedObject.size.y, 0);

            Complete();
        }

        protected override bool Guard()
        {
            return MainModel.GameData.isPerimeterGenerated != true;
        }
    }
}