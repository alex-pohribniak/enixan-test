﻿using Common.Singleton;
using Common.Tasks;
using GameScene.Config;
using GameScene.Enums;
using UnityEngine;

namespace GameScene.Logic
{
    public class DefineAvailableObjectSize : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();

            var initialCounterX = MainModel.GameData.positionForGeneration.x;
            var targetCounterX = MainModel.GameData.positionForGeneration.x +
                                 MainModel.GameData.directionForGeneration.x * GameObjectConfig.MaxXSize;
            

            var initialCounterY = MainModel.GameData.positionForGeneration.y;
            var targetCounterY = MainModel.GameData.positionForGeneration.y +
                                 MainModel.GameData.directionForGeneration.y * GameObjectConfig.MaxYSize;

            
            var availableXSize = 0;
            var availableYSize = 0;

            for (var x = initialCounterX; x < targetCounterX; x++)
            {
                if (x > GameMapConfig.MaxXCoord) break;
                if (MainModel.GameData
                    .gameObjectsData[
                        new Vector3Int(x, MainModel.GameData.positionForGeneration.y, GameMapConfig.DefaultZCoord)]
                    .gameObjectType == GameObjectType.Empty)
                    availableXSize++;
            }

            for (var x = initialCounterX; x > targetCounterX; x--)
            {
                if (x < GameMapConfig.MinXCoord) break;
                if (MainModel.GameData
                    .gameObjectsData[
                        new Vector3Int(x, MainModel.GameData.positionForGeneration.y, GameMapConfig.DefaultZCoord)]
                    .gameObjectType == GameObjectType.Empty)
                    availableXSize++;
            }

            for (var y = initialCounterY; y < targetCounterY; y++)
            {
                if (y > GameMapConfig.MaxYCoord) break;
                if (MainModel.GameData
                    .gameObjectsData[
                        new Vector3Int(MainModel.GameData.positionForGeneration.x, y, GameMapConfig.DefaultZCoord)]
                    .gameObjectType == GameObjectType.Empty)
                    availableYSize++;
            }

            for (var y = initialCounterY; y > targetCounterY; y--)
            {
                if (y < GameMapConfig.MinYCoord) break;
                if (MainModel.GameData
                    .gameObjectsData[
                        new Vector3Int(MainModel.GameData.positionForGeneration.x, y, GameMapConfig.DefaultZCoord)]
                    .gameObjectType == GameObjectType.Empty)
                    availableYSize++;
            }

            if (initialCounterX == targetCounterX) availableXSize = GameObjectConfig.MaxXSize;
            if (initialCounterY == targetCounterY) availableYSize = GameObjectConfig.MaxYSize;

            MainModel.GameData.availableObjectSize = new Vector2Int(availableXSize, availableYSize);


            Complete();
        }

        protected override bool Guard()
        {
            return MainModel.GameData.isPerimeterGenerated != true;
        }
    }
}