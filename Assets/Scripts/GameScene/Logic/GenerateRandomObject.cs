﻿using System.Collections.Generic;
using System.Linq;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using GameScene.Enums;
using GameScene.Model;
using UnityEngine;

namespace GameScene.Logic
{
    public class GenerateRandomObject : BaseTask
    {
        private readonly SimpleDelegate _onGenerateRockComplete;
        private readonly SimpleDelegate _onGenerateTreeComplete;

        public GenerateRandomObject()
        {
            _onGenerateTreeComplete = Complete;
            _onGenerateRockComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();

            var listOfAvailableObjects = new List<GameObjectData>();
            foreach (var key in MainModel.GameData.sizeGameObjectRelation.Keys)
                if (key.x <= MainModel.GameData.availableObjectSize.x &&
                    key.y <= MainModel.GameData.availableObjectSize.y)
                    listOfAvailableObjects.AddRange(MainModel.GameData.sizeGameObjectRelation[key]);


            var randomBool = Random.value < 0.5f;
            GameObjectType randomObjectType;
            if (listOfAvailableObjects.All(element => element.gameObjectType == GameObjectType.Rock))
                randomObjectType = GameObjectType.Rock;
            else if (listOfAvailableObjects.All(element => element.gameObjectType == GameObjectType.Tree))
                randomObjectType = GameObjectType.Tree;
            else
                randomObjectType = randomBool ? GameObjectType.Rock : GameObjectType.Tree;

            var availableObjectsByType = new List<GameObjectData>();
            foreach (var availableObject in listOfAvailableObjects)
                if (availableObject.gameObjectType == randomObjectType)
                    availableObjectsByType.Add(availableObject);


            switch (randomObjectType)
            {
                case GameObjectType.Rock:
                {
                    var randomRockIndex = Random.Range(0, availableObjectsByType.Count);
                    var randomRockPrototype = (RockData) availableObjectsByType[randomRockIndex];
                    var randomRockType = randomRockPrototype.rockType;
                    var randomRock = new RockData(randomRockType);
                    var occupiedTiles = RockData.DefineOccupiedTiles(randomRockType);
                    MainModel.GameData.lastGeneratedObject = randomRock;
                    foreach (var occupiedTile in occupiedTiles)
                        MainModel.GameData.gameObjectsData[occupiedTile] = randomRock;
                    Observer.Emit(GameEvent.GenerateRock, randomRockType, randomRock.uniqueID);
                    break;
                }
                case GameObjectType.Tree:
                {
                    var randomTreeIndex = Random.Range(0, availableObjectsByType.Count);
                    var randomTreePrototype = (TreeData) availableObjectsByType[randomTreeIndex];
                    var randomTreeType = randomTreePrototype.treeType;
                    var randomTree = new TreeData(randomTreeType);
                    var occupiedTiles = TreeData.DefineOccupiedTiles(randomTreeType);
                    MainModel.GameData.lastGeneratedObject = randomTree;
                    foreach (var occupiedTile in occupiedTiles)
                        MainModel.GameData.gameObjectsData[occupiedTile] = randomTree;
                    Observer.Emit(GameEvent.GenerateTree, randomTreeType, randomTree.uniqueID);
                    break;
                }
            }
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(GameEvent.GenerateRockComplete, _onGenerateRockComplete);
            Observer.AddListener(GameEvent.GenerateTreeComplete, _onGenerateTreeComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(GameEvent.GenerateRockComplete, _onGenerateRockComplete);
            Observer.RemoveListener(GameEvent.GenerateTreeComplete, _onGenerateTreeComplete);
        }

        protected override bool Guard()
        {
            return MainModel.GameData.isPerimeterGenerated != true;
        }
    }
}