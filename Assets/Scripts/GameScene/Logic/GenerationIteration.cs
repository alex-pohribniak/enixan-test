﻿using Common.Singleton;
using Common.Tasks;
using GameScene.Enums;

namespace GameScene.Logic
{
    public class GenerationIteration : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(GameEvent.GenerationIteration);
            Complete();
        }
    }
}