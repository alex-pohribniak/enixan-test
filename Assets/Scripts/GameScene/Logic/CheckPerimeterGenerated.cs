﻿using System.Collections.Generic;
using System.Linq;
using Common.Singleton;
using Common.Tasks;
using GameScene.Config;
using GameScene.Enums;
using GameScene.Model;
using UnityEngine;

namespace GameScene.Logic
{
    public class CheckPerimeterGenerated : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var perimeterObjects = new List<GameObjectData>();
            for (var x = GameMapConfig.MinXCoord; x < GameMapConfig.MaxXCoord; x++)
            {
                var tileForCheck = new Vector3Int(x, GameMapConfig.MinYCoord, GameMapConfig.DefaultZCoord);
                perimeterObjects.Add(MainModel.GameData.gameObjectsData[tileForCheck]);

                tileForCheck = new Vector3Int(x, GameMapConfig.MaxYCoord, GameMapConfig.DefaultZCoord);
                perimeterObjects.Add(MainModel.GameData.gameObjectsData[tileForCheck]);
            }

            for (var y = GameMapConfig.MinYCoord; y < GameMapConfig.MaxYCoord; y++)
            {
                var tileForCheck = new Vector3Int(GameMapConfig.MinXCoord, y, GameMapConfig.DefaultZCoord);
                perimeterObjects.Add(MainModel.GameData.gameObjectsData[tileForCheck]);

                tileForCheck = new Vector3Int(GameMapConfig.MaxXCoord, y, GameMapConfig.DefaultZCoord);
                perimeterObjects.Add(MainModel.GameData.gameObjectsData[tileForCheck]);
            }

            if (perimeterObjects.Any(perimeterObject => perimeterObject.gameObjectType == GameObjectType.Empty))
                MainModel.GameData.isPerimeterGenerated = false;
            else
                MainModel.GameData.isPerimeterGenerated = true;

            Complete();
        }
    }
}