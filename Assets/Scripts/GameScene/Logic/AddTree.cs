﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using GameScene.Config;
using GameScene.Enums;
using GameScene.Model;
using UnityEngine;

namespace GameScene.Logic
{
    public class AddTree : BaseTask
    {
        private readonly SimpleDelegate _onGenerateTreeComplete;

        public AddTree()
        {
            _onGenerateTreeComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            var tree = new TreeData(MainModel.GameData.treeForGeneration);
            var occupiedTiles = TreeData.DefineOccupiedTiles(tree.treeType);
            foreach (var occupiedTile in occupiedTiles) MainModel.GameData.gameObjectsData[occupiedTile] = tree;
            Observer.Emit(GameEvent.GenerateTree, tree.treeType, tree.uniqueID);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(GameEvent.GenerateTreeComplete, _onGenerateTreeComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(GameEvent.GenerateTreeComplete, _onGenerateTreeComplete);
        }
    }
}