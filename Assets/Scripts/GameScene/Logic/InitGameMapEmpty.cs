using System.Collections.Generic;
using Common.Singleton;
using Common.Tasks;
using GameScene.Config;
using GameScene.Enums;
using GameScene.Model;
using UnityEngine;

namespace GameScene.Logic
{
    public class InitGameMapEmpty : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();

            MainModel.GameData.gameObjectsData = new Dictionary<Vector3Int, GameObjectData>();

            for (var x = GameMapConfig.MinXCoord; x <= GameMapConfig.MaxXCoord; x++)
            for (var y = GameMapConfig.MinYCoord; y <= GameMapConfig.MaxYCoord; y++)
                MainModel.GameData.gameObjectsData.Add(
                    new Vector3Int(x, y, GameMapConfig.DefaultZCoord),
                    new GameObjectData(GameObjectType.Empty));

            Complete();
        }
    }
}