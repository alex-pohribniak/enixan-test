using System.Collections.Generic;
using System.Linq;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using GameScene.Config;
using GameScene.Enums;
using GameScene.Model;
using UnityEngine;

namespace GameScene.Logic
{
    public class GameLogic : MonoBehaviour
    {
        [SerializeField] private BaseTask startTask;
        [SerializeField] private BaseTask generationIterationTask;
        [SerializeField] private BaseTask delayBetweenIterationTask;
        [SerializeField] private BaseTask showPossibleRockTask;
        [SerializeField] private BaseTask showPossibleTreeTask;
        [SerializeField] private BaseTask addRockTask;
        [SerializeField] private BaseTask addTreeTask;

        private readonly TaskDelegate _onDelayBetweenIterationComplete;

        private readonly SimpleDelegate _onGenerationIteration;
        private readonly TaskDelegate _onGenerationIterationComplete;
        private readonly Vector3IntVector2IntDelegate _onMovePossibleObject;

        private readonly RockTypeDelegate _onShowPossibleRock;
        private readonly TreeTypeDelegate _onShowPossibleTree;
        private readonly SimpleDelegate _onVerifyPossibleRock;
        private readonly SimpleDelegate _onVerifyPossibleTree;

        public GameLogic()
        {
            _onVerifyPossibleTree = OnVerifyPossibleTree;
            _onVerifyPossibleRock = OnVerifyPossibleRock;
            _onMovePossibleObject = OnMovePossibleObject;
            _onShowPossibleRock = OnShowPossibleRock;
            _onShowPossibleTree = OnShowPossibleTree;
            _onDelayBetweenIterationComplete = OnDelayBetweenIterationComplete;
            _onGenerationIterationComplete = OnGenerationIterationComplete;
            _onGenerationIteration = OnGenerationIteration;
        }

        private void Awake()
        {
            Observer.AddListener(GameEvent.GenerationIteration, _onGenerationIteration);
            Observer.AddListener(GameEvent.ShowPossibleRock, _onShowPossibleRock);
            Observer.AddListener(GameEvent.ShowPossibleTree, _onShowPossibleTree);
            Observer.AddListener(GameEvent.MovePossibleObject, _onMovePossibleObject);
            Observer.AddListener(GameEvent.VerifyPossibleRock, _onVerifyPossibleRock);
            Observer.AddListener(GameEvent.VerifyPossibleTree, _onVerifyPossibleTree);

            generationIterationTask.onTaskComplete = _onGenerationIterationComplete;
            delayBetweenIterationTask.onTaskComplete = _onDelayBetweenIterationComplete;
        }

        private void Start()
        {
            startTask.Execute();
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(GameEvent.GenerationIteration, _onGenerationIteration);
            Observer.RemoveListener(GameEvent.ShowPossibleRock, _onShowPossibleRock);
            Observer.RemoveListener(GameEvent.ShowPossibleTree, _onShowPossibleTree);
            Observer.RemoveListener(GameEvent.MovePossibleObject, _onMovePossibleObject);
            Observer.RemoveListener(GameEvent.VerifyPossibleRock, _onVerifyPossibleRock);
            Observer.RemoveListener(GameEvent.VerifyPossibleTree, _onVerifyPossibleTree);
        }

        private void OnVerifyPossibleTree()
        {
            var possibleSize = TreeData.GetSizeByType(MainModel.GameData.treeForGeneration);
            var possibleOccupiedTiles = TreeData.DefineOccupiedTiles(MainModel.GameData.treeForGeneration);
            var gameObjectsData = MainModel.GameData.gameObjectsData;
            var gameObjectsOnSameTiles = new List<GameObjectData>();
            foreach (var key in gameObjectsData.Keys)
            foreach (var possibleOccupiedTile in possibleOccupiedTiles)
                if (key == possibleOccupiedTile)
                    gameObjectsOnSameTiles.Add(gameObjectsData[key]);

            if (gameObjectsOnSameTiles.All(gameObjectData => gameObjectData.gameObjectType == GameObjectType.Empty))
                addTreeTask.Execute();
            else
                Observer.Emit(GameEvent.VerifyFailed);
        }

        private void OnVerifyPossibleRock()
        {
            var possibleSize = RockData.GetSizeByType(MainModel.GameData.rockForGeneration);
            var possibleOccupiedTiles = RockData.DefineOccupiedTiles(MainModel.GameData.rockForGeneration);
            var gameObjectsData = MainModel.GameData.gameObjectsData;
            var gameObjectsOnSameTiles = new List<GameObjectData>();
            foreach (var key in gameObjectsData.Keys)
            foreach (var possibleOccupiedTile in possibleOccupiedTiles)
                if (key == possibleOccupiedTile)
                    gameObjectsOnSameTiles.Add(gameObjectsData[key]);

            if (gameObjectsOnSameTiles.All(gameObjectData => gameObjectData.gameObjectType == GameObjectType.Empty))
                addRockTask.Execute();
            else
                Observer.Emit(GameEvent.VerifyFailed);
        }

        private void OnMovePossibleObject(Vector3Int vector3Int, Vector2Int vector2Int)
        {
            if (vector3Int == Vector3Int.up)
            {
                if (MainModel.GameData.positionForGeneration.y + GameMapConfig.tileSize.y <= GameMapConfig.MaxYCoord)
                    MainModel.GameData.positionForGeneration.y += vector3Int.y;
            }
            else if (vector3Int == Vector3Int.down)
            {
                if (MainModel.GameData.positionForGeneration.y - vector2Int.y >= GameMapConfig.MinYCoord)
                    MainModel.GameData.positionForGeneration.y += vector3Int.y;
            }

            if (vector3Int == Vector3Int.right)
            {
                if (MainModel.GameData.positionForGeneration.x + vector2Int.x <= GameMapConfig.MaxXCoord)
                    MainModel.GameData.positionForGeneration.x += vector3Int.x;
            }
            else if (vector3Int == Vector3Int.left)
            {
                if (MainModel.GameData.positionForGeneration.x - GameMapConfig.tileSize.y >= GameMapConfig.MinXCoord)
                    MainModel.GameData.positionForGeneration.x += vector3Int.x;
            }

            Observer.Emit(GameEvent.MovePossibleObjectComplete);
        }

        private void OnShowPossibleTree(TreeType treeType)
        {
            MainModel.GameData.treeForGeneration = treeType;
            showPossibleTreeTask.Execute();
        }

        private void OnShowPossibleRock(RockType rockType)
        {
            MainModel.GameData.rockForGeneration = rockType;
            showPossibleRockTask.Execute();
        }

        private void OnDelayBetweenIterationComplete(BaseTask task)
        {
            generationIterationTask.Execute();
        }

        private void OnGenerationIterationComplete(BaseTask task)
        {
            if (MainModel.GameData.isPerimeterGenerated == false) delayBetweenIterationTask.Execute();
            else Observer.Emit(GameEvent.PerimeterGenerationComplete);
        }

        private void OnGenerationIteration()
        {
            generationIterationTask.Execute();
        }
    }
}