using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using GameScene.Config;
using GameScene.Enums;
using UnityEngine;

namespace GameScene.Logic
{
    public class ShowPossibleRock : BaseTask
    {
        private readonly SimpleDelegate _onShowPossibleRockComplete;

        public ShowPossibleRock()
        {
            _onShowPossibleRockComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            MainModel.GameData.directionForGeneration = GameMapConfig.directionRightDown;
            MainModel.GameData.positionForGeneration = Vector3Int.zero;
            Observer.Emit(GameEvent.ShowPossibleRockStart);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(GameEvent.ShowPossibleRockComplete, _onShowPossibleRockComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(GameEvent.ShowPossibleRockComplete, _onShowPossibleRockComplete);
        }
    }
}