﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using GameScene.Enums;
using GameScene.Model;

namespace GameScene.Logic
{
    public class AddRock : BaseTask
    {
        private readonly SimpleDelegate _onGenerateRockComplete;

        public AddRock()
        {
            _onGenerateRockComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            var rock = new RockData(MainModel.GameData.rockForGeneration);
            var occupiedTiles = RockData.DefineOccupiedTiles(rock.rockType);
            foreach (var occupiedTile in occupiedTiles) MainModel.GameData.gameObjectsData[occupiedTile] = rock;
            Observer.Emit(GameEvent.GenerateRock, rock.rockType, rock.uniqueID);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(GameEvent.GenerateRockComplete, _onGenerateRockComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(GameEvent.GenerateRockComplete, _onGenerateRockComplete);
        }
    }
}