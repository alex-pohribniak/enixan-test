﻿using System.Collections.Generic;
using Common.Singleton;
using Common.Tasks;
using GameScene.Config;
using GameScene.Enums;
using GameScene.Model;
using UnityEngine;

namespace GameScene.Logic
{
    public class InitSizeGameObjectsRelation : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            MainModel.GameData.sizeGameObjectRelation = new Dictionary<Vector2Int, List<GameObjectData>>();

            MainModel.GameData.sizeGameObjectRelation.Add(SizeConfig.size1X1,
                new List<GameObjectData> {new TreeData(TreeType.One)});

            MainModel.GameData.sizeGameObjectRelation.Add(SizeConfig.size2X2,
                new List<GameObjectData>
                {
                    new TreeData(TreeType.Two),
                    new TreeData(TreeType.Three),
                    new TreeData(TreeType.Four),
                    new TreeData(TreeType.Five),
                    new TreeData(TreeType.Six),

                    new RockData(RockType.One)
                });

            MainModel.GameData.sizeGameObjectRelation.Add(SizeConfig.size3X2,
                new List<GameObjectData>
                {
                    new RockData(RockType.Five)
                });

            MainModel.GameData.sizeGameObjectRelation.Add(SizeConfig.size3X3,
                new List<GameObjectData>
                {
                    new TreeData(TreeType.Seven),

                    new RockData(RockType.Two),
                    new RockData(RockType.Three),
                    new RockData(RockType.Four)
                });

            MainModel.GameData.sizeGameObjectRelation.Add(SizeConfig.size4X3,
                new List<GameObjectData>
                {
                    new RockData(RockType.Zero)
                });

            MainModel.GameData.sizeGameObjectRelation.Add(SizeConfig.size6X6,
                new List<GameObjectData>
                {
                    new TreeData(TreeType.Zero)
                });

            Complete();
        }
    }
}