using Common.Singleton;
using Common.Tasks;
using GameScene.Enums;

namespace GameScene.Logic
{
    public class AddObjectComplete : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(GameEvent.AddObjectComplete);
            Complete();
        }
    }
}