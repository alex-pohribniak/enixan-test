using System.Globalization;
using Common.Enums;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace LoadingScene.View
{
    public class AsyncSceneLoader : MonoBehaviour
    {
        public Slider progressBar;
        public Text progressCounter;
        private AsyncOperation _loadingAsyncOperation;

        private void Start()
        {
            _loadingAsyncOperation = SceneManager.LoadSceneAsync(Scenes.GameScene);
        }

        private void Update()
        {
            var progressBarValue = Mathf.Clamp01(_loadingAsyncOperation.progress / 0.9f);
            progressBar.value = progressBarValue;
            progressCounter.text = $"{Mathf.Round(progressBarValue * 100).ToString(CultureInfo.InvariantCulture)} %";
        }
    }
}