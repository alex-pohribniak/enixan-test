﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace Common.Logic
{
    public class FadeScreen : BaseTask
    {
        private readonly SimpleDelegate _onFadeScreenComplete;

        public FadeScreen()
        {
            _onFadeScreenComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(CommonEvent.FadeScreen);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(CommonEvent.FadeScreenComplete, _onFadeScreenComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(CommonEvent.FadeScreenComplete, _onFadeScreenComplete);
        }
    }
}