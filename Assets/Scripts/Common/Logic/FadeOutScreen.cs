﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace Common.Logic
{
    public class FadeOutScreen : BaseTask
    {
        private readonly SimpleDelegate _onFadeOutScreenComplete;

        public FadeOutScreen()
        {
            _onFadeOutScreenComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(CommonEvent.FadeOutScreen);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(CommonEvent.FadeOutScreenComplete, _onFadeOutScreenComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(CommonEvent.FadeOutScreenComplete, _onFadeOutScreenComplete);
        }
    }
}