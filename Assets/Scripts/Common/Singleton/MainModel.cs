﻿using System;
using GameScene.Model;
using UnityEngine;

namespace Common.Singleton
{
    public class MainModel : MonoBehaviour
    {
        private static MainModel _mainModel;
        private GameData _gameData;

        private static MainModel Instance
        {
            get
            {
                if (_mainModel != null) return _mainModel;
                _mainModel = FindObjectOfType(typeof(MainModel)) as MainModel;
                if (_mainModel == null)
                    throw new Exception("There needs to be one active Model script in your scene.");
                _mainModel.Init();

                return _mainModel;
            }
        }

        public static GameData GameData
        {
            get => Instance._gameData;
            set => Instance._gameData = value;
        }

        public static bool IsNull()
        {
            return _mainModel == null;
        }

        private void Init()
        {
            if (_gameData == null) _gameData = new GameData();
        }
    }
}