﻿namespace Common.Enums
{
    public static class Scenes
    {
        public const string LoadingScene = "LoadingScene";
        public const string GameScene = "GameScene";
    }
}