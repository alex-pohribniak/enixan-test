using Common.Tasks;
using UnityEngine;

namespace Common.Enums
{
    public delegate void SimpleDelegate();

    public delegate int ReturnIntSimpleDelegate();

    public delegate void TaskDelegate(BaseTask task);

    public delegate void BoolDelegate(bool value);

    public delegate void IntDelegate(int value);

    public delegate void IntIntDelegate(int value1, int value2);

    public delegate void FloatDelegate(float value);

    public delegate void StringDelegate(string value);

    public delegate void Vector2IntDelegate(Vector2Int position);

    public delegate void Vector3Delegate(Vector3 position);
}