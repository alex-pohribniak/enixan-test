﻿namespace Common.Enums
{
    public static class CommonEvent
    {
        public const string LockUserInterface = "LockUserInterface";
        public const string UnLockUserInterface = "UnLockUserInterface";
        
        public const string FadeScreen = "FadeScreen";
        public const string FadeScreenComplete = "FadeScreenComplete";
        public const string FadeOutScreen = "FadeOutScreen";
        public const string FadeOutScreenComplete = "FadeOutScreenComplete";
    }
}