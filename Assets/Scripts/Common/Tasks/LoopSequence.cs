﻿namespace Common.Tasks
{
    public class LoopSequence : SequenceTask
    {
        public override void Complete()
        {
            base.Complete();
            Execute();
        }
    }
}