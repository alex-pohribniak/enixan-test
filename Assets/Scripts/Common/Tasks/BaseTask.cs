﻿using Common.Enums;
using UnityEngine;

namespace Common.Tasks
{
    public class BaseTask : MonoBehaviour
    {
        private float _startTime;
        protected internal bool forceMode;
        public bool logging;
        public TaskDelegate onTaskComplete;
        public TaskDelegate onTaskGuard;
        public bool inProgress { get; private set; }

        public virtual void ForceModeOn()
        {
            forceMode = true;
            if (logging) print($"{name}: force mode ON, time: {Time.time - _startTime}s");
            if (inProgress) ForceComplete();
        }

        protected virtual void Activate()
        {
            if (logging)
            {
                _startTime = Time.time;
                print(forceMode ? $"{name}: activate force" : $"{name}: activate");
            }

            inProgress = true;
        }

        protected virtual void DeActivate()
        {
            if (logging) print($"{name}: de_activate, life time: {Time.time - _startTime}s");

            inProgress = false;
            forceMode = false;
        }

        protected virtual void ForceComplete()
        {
        }

        public void Execute()
        {
            if (inProgress) return;
            if (Guard())
            {
                if (forceMode)
                    OnForceExecute();
                else
                    OnExecute();
            }
            else
            {
                OnGuard();
            }
        }

        protected virtual bool Guard()
        {
            return true;
        }

        protected virtual void OnGuard()
        {
            if (logging) print($"{name}: on_guard");

            DeActivate();
            onTaskGuard?.DynamicInvoke(this);
        }

        public virtual void Complete()
        {
            DeActivate();
            onTaskComplete?.DynamicInvoke(this);
        }

        public void Terminate()
        {
            if (inProgress) OnTerminate();
        }

        protected internal virtual void OnTerminate()
        {
            DeActivate();
        }

        protected virtual void OnExecute()
        {
            Activate();
        }

        protected virtual void OnForceExecute()
        {
            Activate();
        }
    }
}