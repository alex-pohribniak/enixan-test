﻿using Common.Enums;
using Common.Singleton;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Common.View
{
    public class FadeScreenView : MonoBehaviour
    {
        public Image fadeScreen;
        [SerializeField] private float animationDuration;
        private readonly SimpleDelegate _onFadeOutScreen;
        private readonly SimpleDelegate _onFadeScreen;
        private Sequence _animationSequence;

        public FadeScreenView()
        {
            _onFadeScreen = OnFadeScreen;
            _onFadeOutScreen = OnFadeOutScreen;
        }

        protected void Awake()
        {
            fadeScreen.gameObject.SetActive(false);

            Observer.AddListener(CommonEvent.FadeScreen, _onFadeScreen);
            Observer.AddListener(CommonEvent.FadeOutScreen, _onFadeOutScreen);
        }

        protected void OnDestroy()
        {
            if (Observer.IsNull()) return;

            Observer.RemoveListener(CommonEvent.FadeScreen, _onFadeScreen);
            Observer.RemoveListener(CommonEvent.FadeOutScreen, _onFadeOutScreen);
        }

        private void ClearTween()
        {
            if (_animationSequence == null) return;
            _animationSequence.Kill();
            _animationSequence = null;
        }

        private void OnFadeScreen()
        {
            ClearTween();

            fadeScreen.gameObject.SetActive(true);
            fadeScreen.color = Color.black;

            animationDuration = 1f;
            _animationSequence = DOTween.Sequence();
            var tweenFade = fadeScreen.DOFade(1f, animationDuration);
            _animationSequence.Append(tweenFade);
            _animationSequence.OnComplete(() => { Observer.Emit(CommonEvent.FadeScreenComplete); });
            _animationSequence.Play();
        }

        private void OnFadeOutScreen()
        {
            ClearTween();

            animationDuration = 1f;
            _animationSequence = DOTween.Sequence();
            var tweenFade = fadeScreen.DOFade(0f, animationDuration);
            _animationSequence.Append(tweenFade);
            _animationSequence.OnComplete(() =>
            {
                fadeScreen.gameObject.SetActive(false);
                Observer.Emit(CommonEvent.FadeOutScreenComplete);
            });
            _animationSequence.Play();
        }
    }
}