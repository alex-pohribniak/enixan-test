﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.Vector3 UnityEngine.GridLayout::CellToLocalInterpolated(UnityEngine.Vector3)
extern void GridLayout_CellToLocalInterpolated_m0C175A8A30A121F9852CE54F83989AC42CBD6C00 (void);
// 0x00000002 UnityEngine.Vector3 UnityEngine.GridLayout::LocalToWorld(UnityEngine.Vector3)
extern void GridLayout_LocalToWorld_m51CF5FE39EF2DAABD8D034A983BC4D014D7D6D95 (void);
// 0x00000003 System.Void UnityEngine.GridLayout::DoNothing()
extern void GridLayout_DoNothing_m36201F4787709460C994921B16CAC95CD490E0A1 (void);
// 0x00000004 System.Void UnityEngine.GridLayout::CellToLocalInterpolated_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void GridLayout_CellToLocalInterpolated_Injected_m81BDFDF4DB1A95D1D19194DADD9CA3A708D4F314 (void);
// 0x00000005 System.Void UnityEngine.GridLayout::LocalToWorld_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void GridLayout_LocalToWorld_Injected_m793C4D10551BDD34F7DCD31F86E87BA1C876AB8E (void);
static Il2CppMethodPointer s_methodPointers[5] = 
{
	GridLayout_CellToLocalInterpolated_m0C175A8A30A121F9852CE54F83989AC42CBD6C00,
	GridLayout_LocalToWorld_m51CF5FE39EF2DAABD8D034A983BC4D014D7D6D95,
	GridLayout_DoNothing_m36201F4787709460C994921B16CAC95CD490E0A1,
	GridLayout_CellToLocalInterpolated_Injected_m81BDFDF4DB1A95D1D19194DADD9CA3A708D4F314,
	GridLayout_LocalToWorld_Injected_m793C4D10551BDD34F7DCD31F86E87BA1C876AB8E,
};
static const int32_t s_InvokerIndices[5] = 
{
	1167,
	1167,
	1417,
	734,
	734,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_GridModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_GridModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_GridModule_CodeGenModule = 
{
	"UnityEngine.GridModule.dll",
	5,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_GridModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
