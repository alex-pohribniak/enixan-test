﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void LoadingScene.View.AsyncSceneLoader::Start()
extern void AsyncSceneLoader_Start_m53F804786571201F7BDE310B292D22BF947B29A3 (void);
// 0x00000002 System.Void LoadingScene.View.AsyncSceneLoader::Update()
extern void AsyncSceneLoader_Update_m095776B9718DA0D2C2730A7FF4B0C1E16EBA1761 (void);
// 0x00000003 System.Void LoadingScene.View.AsyncSceneLoader::.ctor()
extern void AsyncSceneLoader__ctor_mDB001A538CE6A6FCA45D036F886C1E982716A5A9 (void);
// 0x00000004 System.Void GameScene.View.AddControlSegment::.ctor()
extern void AddControlSegment__ctor_m0167FE6EB9C17C9F71C2229E27F3ADE9EC51361D (void);
// 0x00000005 System.Void GameScene.View.AddControlSegment::OnPerimeterGenerationComplete()
extern void AddControlSegment_OnPerimeterGenerationComplete_m18E9C32A60E3668B15F84CEB6AD45997B22981BF (void);
// 0x00000006 System.Void GameScene.View.AddControlSegment::Awake()
extern void AddControlSegment_Awake_mADA80BE9079670601EAE9D21C2322048E6B93996 (void);
// 0x00000007 System.Void GameScene.View.AddControlSegment::OnDestroy()
extern void AddControlSegment_OnDestroy_mD9391CFC172FF52243EE64A651DB1B0E4A31AC81 (void);
// 0x00000008 System.Void GameScene.View.AddControlSegment::OnGetTreeInfo(GameScene.View.TreeView)
extern void AddControlSegment_OnGetTreeInfo_m878054E5A5E7DCF0FE70D3BCB8FF8B0AEF8141CA (void);
// 0x00000009 System.Void GameScene.View.AddControlSegment::OnGetRockInfo(GameScene.View.RockView)
extern void AddControlSegment_OnGetRockInfo_mAB54DB33B99FE6A8F3F41D31EB6569416C859B66 (void);
// 0x0000000A System.Void GameScene.View.AddControlSegment::OnVerifyFailed()
extern void AddControlSegment_OnVerifyFailed_m75A02685C47EB564E1811585A589B704D884F5D8 (void);
// 0x0000000B System.Void GameScene.View.AddControlSegment::ResetInfoText()
extern void AddControlSegment_ResetInfoText_m3F93E3417F9959F8D9A595F0ED322F276DF1B43D (void);
// 0x0000000C System.Void GameScene.View.AddControlSegment::OnAddObjectComplete()
extern void AddControlSegment_OnAddObjectComplete_m16C2CE810E3431F8B5508D11A1564032825385D2 (void);
// 0x0000000D System.Void GameScene.View.AddControlSegment::OnShowPossibleTree(GameScene.Enums.TreeType)
extern void AddControlSegment_OnShowPossibleTree_m926DF99B7EB184908975F1011071427115FCE237 (void);
// 0x0000000E System.Void GameScene.View.AddControlSegment::OnShowPossibleRock(GameScene.Enums.RockType)
extern void AddControlSegment_OnShowPossibleRock_m2F07B0A2172CE1B707E9AD75F1CC2990795C9F16 (void);
// 0x0000000F System.Void GameScene.View.AddControlSegment::Accept()
extern void AddControlSegment_Accept_m2C023FB6C1488B0AE618239FE8686306FA5A3F3A (void);
// 0x00000010 System.Void GameScene.View.AddControlSegment::Cancel()
extern void AddControlSegment_Cancel_m7C5D1FC9D38261A9E92740048C5DA77BC4787B5B (void);
// 0x00000011 System.Void GameScene.View.AddControlSegment::ActivationSwap()
extern void AddControlSegment_ActivationSwap_m6591E6144166C3622E742F37483B27DE75C27C7B (void);
// 0x00000012 System.Void GameScene.View.CameraController::Start()
extern void CameraController_Start_mE04AA0B2A1B81955FAAF5FD2A4DBBA22B61294FA (void);
// 0x00000013 System.Void GameScene.View.CameraController::Update()
extern void CameraController_Update_m7AC3B3CBCF845D071DAE7334957547734CCF9C82 (void);
// 0x00000014 System.Void GameScene.View.CameraController::CheckObjectInfo(UnityEngine.Vector3)
extern void CameraController_CheckObjectInfo_m72AAE3E912CB97CA2C97A1CCD470BDCEE0710541 (void);
// 0x00000015 System.Void GameScene.View.CameraController::.ctor()
extern void CameraController__ctor_mC5858AFEA543DF84156A102F625A5774DBA7043E (void);
// 0x00000016 System.Void GameScene.View.MoveButton::.ctor()
extern void MoveButton__ctor_m9CA6A1CFBB036D5C799D94A0945D8705445AFE9A (void);
// 0x00000017 System.Void GameScene.View.MoveButton::Awake()
extern void MoveButton_Awake_m6FFCDCB3F414E2F675DEF6029A64115A96B25066 (void);
// 0x00000018 System.Void GameScene.View.MoveButton::OnDestroy()
extern void MoveButton_OnDestroy_mEC61C3BFC7E776DEED876A6787B7E83C69B92937 (void);
// 0x00000019 System.Void GameScene.View.MoveButton::OnShowPossibleTreeComplete()
extern void MoveButton_OnShowPossibleTreeComplete_m3273A6C8DA1EC81BCC841E9507CF11E1CAC2EA4C (void);
// 0x0000001A System.Void GameScene.View.MoveButton::OnShowPossibleRockComplete()
extern void MoveButton_OnShowPossibleRockComplete_m1AD7E6E6BBB1C79A08CBF26A7191AF74F5F78B5A (void);
// 0x0000001B System.Void GameScene.View.MoveButton::Move()
extern void MoveButton_Move_m44D188154B4446A04BB07227C1C52BC162FA32C1 (void);
// 0x0000001C System.Void GameScene.View.ObjectPool::.ctor()
extern void ObjectPool__ctor_mABE989CA0A63A7924799A5B0E597CCC9E0F412F5 (void);
// 0x0000001D System.Void GameScene.View.PossibleRock::.ctor()
extern void PossibleRock__ctor_m47F3F1870CEE7BCD52D8FFE3E79D3EE7E5B4560C (void);
// 0x0000001E System.Void GameScene.View.PossibleRock::Awake()
extern void PossibleRock_Awake_m65CF3FFFD2F5FF27E74BF5367098162A4D42E7FC (void);
// 0x0000001F System.Void GameScene.View.PossibleRock::OnDestroy()
extern void PossibleRock_OnDestroy_m2383BBA41CBA7A2C060B2CD5BC0CD5171F062B04 (void);
// 0x00000020 System.Void GameScene.View.PossibleRock::OnAddObjectComplete()
extern void PossibleRock_OnAddObjectComplete_m241307C9D8F12080A80F5A2277B7BB2549AEF17A (void);
// 0x00000021 System.Void GameScene.View.PossibleRock::OnAcceptPossibleObject()
extern void PossibleRock_OnAcceptPossibleObject_mE06ADDB3164DC66538E65F8C3FAAABA0090798B7 (void);
// 0x00000022 System.Void GameScene.View.PossibleRock::OnCancelPossibleObject()
extern void PossibleRock_OnCancelPossibleObject_mB31DE06C0DB1A7DD3AFA57C107CFF2CEAEADBF58 (void);
// 0x00000023 System.Void GameScene.View.PossibleRock::DestroyRock()
extern void PossibleRock_DestroyRock_mF1E79A510E88E229BCD519BE32D8C9B1F9135D30 (void);
// 0x00000024 System.Void GameScene.View.PossibleRock::OnMovePossibleObjectComplete()
extern void PossibleRock_OnMovePossibleObjectComplete_m75E92DA696F041DC49DB569A6238C4D88A5EF6EE (void);
// 0x00000025 System.Void GameScene.View.PossibleRock::OnShowPossibleRockStart()
extern void PossibleRock_OnShowPossibleRockStart_m3812B947411202F1422CDB8FD858E204EA0FE256 (void);
// 0x00000026 System.Void GameScene.View.PossibleRock::PositionRock()
extern void PossibleRock_PositionRock_mCB013D1BE03A110C166DE81CAE935C161B4ECBA4 (void);
// 0x00000027 UnityEngine.Vector3 GameScene.View.PossibleRock::<PositionRock>b__16_0(UnityEngine.Vector3Int)
extern void PossibleRock_U3CPositionRockU3Eb__16_0_m01C22BB83342951BA5A2329666AB59346C2BE414 (void);
// 0x00000028 System.Void GameScene.View.PossibleTree::.ctor()
extern void PossibleTree__ctor_m420FB70CA9B2A7E2D075F5E1137737E0FFE17DF0 (void);
// 0x00000029 System.Void GameScene.View.PossibleTree::Awake()
extern void PossibleTree_Awake_mF601F9F5004E0B9041880711358C23AB55B4BDA6 (void);
// 0x0000002A System.Void GameScene.View.PossibleTree::OnDestroy()
extern void PossibleTree_OnDestroy_m9C25462AF736E27AC40B685C8CDC7ADB97E316EE (void);
// 0x0000002B System.Void GameScene.View.PossibleTree::OnAcceptPossibleObject()
extern void PossibleTree_OnAcceptPossibleObject_mF7296996385CC8BF25419DFBC4E0F31FF08AC1C0 (void);
// 0x0000002C System.Void GameScene.View.PossibleTree::OnAddObjectComplete()
extern void PossibleTree_OnAddObjectComplete_mBA2A54D1187D9F1FFDF47ED2D8A24A58826C00AA (void);
// 0x0000002D System.Void GameScene.View.PossibleTree::DestroyTree()
extern void PossibleTree_DestroyTree_m3BC9F12E7F1560578D9AC8417473DC96878D2FC4 (void);
// 0x0000002E System.Void GameScene.View.PossibleTree::OnCancelPossibleObject()
extern void PossibleTree_OnCancelPossibleObject_m9BCA729794231453DFEF55432762E21FAB35C3D5 (void);
// 0x0000002F System.Void GameScene.View.PossibleTree::OnMovePossibleObjectComplete()
extern void PossibleTree_OnMovePossibleObjectComplete_m309045EC5D1BDEE348F1C79BA0A5C1AEB2C24141 (void);
// 0x00000030 System.Void GameScene.View.PossibleTree::OnShowPossibleTreeStart()
extern void PossibleTree_OnShowPossibleTreeStart_m8CA98F71A9C01FC9D5ED4EFFD575A92C8E35D814 (void);
// 0x00000031 System.Void GameScene.View.PossibleTree::PositionTree()
extern void PossibleTree_PositionTree_m67F1EFF928AD79754014DD2F23F6788AA3453007 (void);
// 0x00000032 UnityEngine.Vector3 GameScene.View.PossibleTree::<PositionTree>b__16_0(UnityEngine.Vector3Int)
extern void PossibleTree_U3CPositionTreeU3Eb__16_0_mB776B8B8860390BA15E01B559F0DF54894B02BE2 (void);
// 0x00000033 System.Void GameScene.View.RockGenerator::.ctor()
extern void RockGenerator__ctor_m978A91D4EA3E32D440A98672A640DBA88C6134C7 (void);
// 0x00000034 System.Void GameScene.View.RockGenerator::Awake()
extern void RockGenerator_Awake_m53E7AA0224555A0841F6DCD9D84C10977712A372 (void);
// 0x00000035 System.Void GameScene.View.RockGenerator::OnDestroy()
extern void RockGenerator_OnDestroy_m3B8389561A1C0514342CD0DCD15C798077B5F241 (void);
// 0x00000036 System.Void GameScene.View.RockGenerator::OnGenerateRock(GameScene.Enums.RockType,System.Guid)
extern void RockGenerator_OnGenerateRock_mBCD7EB89CBB178C5738E62F42D29F658207C5C43 (void);
// 0x00000037 UnityEngine.Vector3 GameScene.View.RockGenerator::<OnGenerateRock>b__5_0(UnityEngine.Vector3Int)
extern void RockGenerator_U3COnGenerateRockU3Eb__5_0_m5D761C18C6D34F9278E850AD7D6EE3EBE0C3CFF8 (void);
// 0x00000038 System.Void GameScene.View.RockObjectPool::.ctor()
extern void RockObjectPool__ctor_mC7C90EC2270E6115FEDEB60296DEE2A17FED8353 (void);
// 0x00000039 System.Void GameScene.View.RockPreview::AddRock()
extern void RockPreview_AddRock_m2B70EFF2658753AE2655435327FE3CF36303FE02 (void);
// 0x0000003A System.Void GameScene.View.RockPreview::.ctor()
extern void RockPreview__ctor_m56926744183297F943C655D9C98CA254E98D0CF8 (void);
// 0x0000003B System.Void GameScene.View.RockView::.ctor()
extern void RockView__ctor_m7F1D7A825523AB9892D65B6BC1C272811B46DBCD (void);
// 0x0000003C System.Void GameScene.View.RocksObjectPools::Awake()
extern void RocksObjectPools_Awake_mF441324B6498DC15EDBE2CB30DEC5FDF718B73CE (void);
// 0x0000003D System.Void GameScene.View.RocksObjectPools::Start()
extern void RocksObjectPools_Start_m639B8F40D52F0606C9C2989EB14D552B30975028 (void);
// 0x0000003E UnityEngine.GameObject GameScene.View.RocksObjectPools::GetPooledObject(GameScene.Enums.RockType)
extern void RocksObjectPools_GetPooledObject_mF0FE6305A3F4CBD9E0BF7DA3BB4346831BD52E76 (void);
// 0x0000003F System.Void GameScene.View.RocksObjectPools::PutBackPooledObject(UnityEngine.GameObject)
extern void RocksObjectPools_PutBackPooledObject_m6A8D92AC4CF5CF78E1ED5E2109A357B937CF3255 (void);
// 0x00000040 System.Void GameScene.View.RocksObjectPools::.ctor()
extern void RocksObjectPools__ctor_m77CD66E45F83B3BB6DC2DA170DCFAA372CE29B15 (void);
// 0x00000041 System.Void GameScene.View.RocksObjectPools/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m66706973119C8C8478FC96E1D11D7D529D276928 (void);
// 0x00000042 System.Boolean GameScene.View.RocksObjectPools/<>c__DisplayClass4_0::<GetPooledObject>b__0(GameScene.View.RockObjectPool)
extern void U3CU3Ec__DisplayClass4_0_U3CGetPooledObjectU3Eb__0_m6AA210BEEAB8FAF911B98D87759F26520312A730 (void);
// 0x00000043 System.Void GameScene.View.TileOutline::ChangeVision()
extern void TileOutline_ChangeVision_m6C358F9F3686C6E6EA1D72F4CD3F6E935BACB890 (void);
// 0x00000044 System.Void GameScene.View.TileOutline::.ctor()
extern void TileOutline__ctor_mE2A62F588012B218D33BBCC29D11ADBF697F00AE (void);
// 0x00000045 System.Void GameScene.View.TreeGenerator::.ctor()
extern void TreeGenerator__ctor_m9BFB608E04BF29E8663D7BC9F01215132137F75A (void);
// 0x00000046 System.Void GameScene.View.TreeGenerator::Awake()
extern void TreeGenerator_Awake_m26AF1CEF65411DBB8DF605E6DB30319361DAB104 (void);
// 0x00000047 System.Void GameScene.View.TreeGenerator::OnDestroy()
extern void TreeGenerator_OnDestroy_m4BD50FCDF893AB107965BFA323D91685B04B0F14 (void);
// 0x00000048 System.Void GameScene.View.TreeGenerator::OnGenerateTree(GameScene.Enums.TreeType,System.Guid)
extern void TreeGenerator_OnGenerateTree_m4A989E8DD552360166730BFF7AD6601D74B03587 (void);
// 0x00000049 UnityEngine.Vector3 GameScene.View.TreeGenerator::<OnGenerateTree>b__5_0(UnityEngine.Vector3Int)
extern void TreeGenerator_U3COnGenerateTreeU3Eb__5_0_m2FC9D272A4005F560A0129A93EAAD5C4B5EC5AD4 (void);
// 0x0000004A System.Void GameScene.View.TreeObjectPool::.ctor()
extern void TreeObjectPool__ctor_m39CF883D1BAF86696B98E430491506EF16C17DE5 (void);
// 0x0000004B System.Void GameScene.View.TreePreview::AddTree()
extern void TreePreview_AddTree_m55464ACC0FBA33FFB1F5637DF7822C7578AB2D7A (void);
// 0x0000004C System.Void GameScene.View.TreePreview::.ctor()
extern void TreePreview__ctor_m7496D05607C635A02CF0DD0A138A694C8400C3C7 (void);
// 0x0000004D System.Void GameScene.View.TreeView::.ctor()
extern void TreeView__ctor_mDA69663D1436EE204210A26D082F0F6299CD59A9 (void);
// 0x0000004E System.Void GameScene.View.TreesObjectPools::Awake()
extern void TreesObjectPools_Awake_m1704B8F431BD888984674469C1E440CFDA630D06 (void);
// 0x0000004F System.Void GameScene.View.TreesObjectPools::Start()
extern void TreesObjectPools_Start_m5C0648B4B7450CA1B8009BF26F0C420C7A7D42FE (void);
// 0x00000050 UnityEngine.GameObject GameScene.View.TreesObjectPools::GetPooledObject(GameScene.Enums.TreeType)
extern void TreesObjectPools_GetPooledObject_m703667CC836752AA4C39DA5AD3273359996D260D (void);
// 0x00000051 System.Void GameScene.View.TreesObjectPools::PutBackPooledObject(UnityEngine.GameObject)
extern void TreesObjectPools_PutBackPooledObject_m8925FCD834954A1EBB75E7968F4D44742085C400 (void);
// 0x00000052 System.Void GameScene.View.TreesObjectPools::.ctor()
extern void TreesObjectPools__ctor_m543A2FB862D8CDDDC50FA8FAF453392C546412DC (void);
// 0x00000053 System.Void GameScene.View.TreesObjectPools/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m9CD88BA7E34F423CB602F2F7B53198EFCE638ED7 (void);
// 0x00000054 System.Boolean GameScene.View.TreesObjectPools/<>c__DisplayClass4_0::<GetPooledObject>b__0(GameScene.View.TreeObjectPool)
extern void U3CU3Ec__DisplayClass4_0_U3CGetPooledObjectU3Eb__0_m1438ED2504569B257CC8D6EA434A47295F8D6217 (void);
// 0x00000055 GameScene.Model.GameObjectData GameScene.Model.GameData::FindGameObjectDataByUniqueID(System.Guid)
extern void GameData_FindGameObjectDataByUniqueID_mCFFC2D2B025ED5C864E45E6F23D3A147A5305787 (void);
// 0x00000056 System.Collections.Generic.List`1<UnityEngine.Vector3Int> GameScene.Model.GameData::FindOccupiedTilesByUniqueID(System.Guid)
extern void GameData_FindOccupiedTilesByUniqueID_m364B2EA91A7F155688424388B5EE08336997BE26 (void);
// 0x00000057 System.Void GameScene.Model.GameData::.ctor()
extern void GameData__ctor_m7633BB28CB93BBCAAE513D18E7D242DE61853BEB (void);
// 0x00000058 System.Void GameScene.Model.GameData::.cctor()
extern void GameData__cctor_m523205A9D014BB669C839EEBCEB3F7EFFB43CB2C (void);
// 0x00000059 System.Void GameScene.Model.GameData/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m7B8547AB811C38DE96B61EA6A5E2CCB52E53AF78 (void);
// 0x0000005A System.Boolean GameScene.Model.GameData/<>c__DisplayClass10_0::<FindGameObjectDataByUniqueID>b__0(System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3Int,GameScene.Model.GameObjectData>)
extern void U3CU3Ec__DisplayClass10_0_U3CFindGameObjectDataByUniqueIDU3Eb__0_mB12AA113EDD45B50B3535FD1064840C70BEFFC44 (void);
// 0x0000005B System.Void GameScene.Model.GameData/<>c::.cctor()
extern void U3CU3Ec__cctor_m42DF686420142647887B5D78F67CEF80AD14DF46 (void);
// 0x0000005C System.Void GameScene.Model.GameData/<>c::.ctor()
extern void U3CU3Ec__ctor_m9E3F20B7E5E75A981EBB6996638ABB039D2B3425 (void);
// 0x0000005D GameScene.Model.GameObjectData GameScene.Model.GameData/<>c::<FindGameObjectDataByUniqueID>b__10_1(System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3Int,GameScene.Model.GameObjectData>)
extern void U3CU3Ec_U3CFindGameObjectDataByUniqueIDU3Eb__10_1_mFFE91556070B70D04097125B4AD5B5A8C85277AA (void);
// 0x0000005E UnityEngine.Vector3Int GameScene.Model.GameData/<>c::<FindOccupiedTilesByUniqueID>b__11_1(System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3Int,GameScene.Model.GameObjectData>)
extern void U3CU3Ec_U3CFindOccupiedTilesByUniqueIDU3Eb__11_1_m1B797AC68F76B6B56EFB1EC7CFD2C454387BE5B1 (void);
// 0x0000005F System.Void GameScene.Model.GameData/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m215085D56AB8D943DEE1502DF70B8E94DE61AD20 (void);
// 0x00000060 System.Boolean GameScene.Model.GameData/<>c__DisplayClass11_0::<FindOccupiedTilesByUniqueID>b__0(System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector3Int,GameScene.Model.GameObjectData>)
extern void U3CU3Ec__DisplayClass11_0_U3CFindOccupiedTilesByUniqueIDU3Eb__0_m98B99F88F16DA7964C4A34B96D29BCCB64C35797 (void);
// 0x00000061 System.Void GameScene.Model.GameObjectData::.ctor()
extern void GameObjectData__ctor_mBF438C9887A5D44B7D5A4A1BC897265C9BDB8DA8 (void);
// 0x00000062 System.Void GameScene.Model.GameObjectData::.ctor(GameScene.Enums.GameObjectType)
extern void GameObjectData__ctor_m90F65DC8E9A35658BD2FCCEA7E488ACF5DCD7454 (void);
// 0x00000063 System.Void GameScene.Model.RockData::.ctor(GameScene.Enums.RockType)
extern void RockData__ctor_m0AF954A9DF11FED6D4E6574F3B0603656EEB7B05 (void);
// 0x00000064 UnityEngine.Vector2Int GameScene.Model.RockData::GetSizeByType(GameScene.Enums.RockType)
extern void RockData_GetSizeByType_mD97AC797676084C5348106C53E61A6E7E5B6082F (void);
// 0x00000065 System.Collections.Generic.List`1<UnityEngine.Vector3Int> GameScene.Model.RockData::DefineOccupiedTiles(GameScene.Enums.RockType)
extern void RockData_DefineOccupiedTiles_m40F51F8C8297FEBE93BF87002A8B425C3F2E31A6 (void);
// 0x00000066 System.Void GameScene.Model.TreeData::.ctor(GameScene.Enums.TreeType)
extern void TreeData__ctor_mAB813EFAFB5337DD07890173C52571DCE7820ED4 (void);
// 0x00000067 UnityEngine.Vector2Int GameScene.Model.TreeData::GetSizeByType(GameScene.Enums.TreeType)
extern void TreeData_GetSizeByType_m85CB220415FAFB9E0F9DD27C2536C72189DF30DF (void);
// 0x00000068 System.Collections.Generic.List`1<UnityEngine.Vector3Int> GameScene.Model.TreeData::DefineOccupiedTiles(GameScene.Enums.TreeType)
extern void TreeData_DefineOccupiedTiles_mE2E28371CF04B261B81690FC047F4B961F78D300 (void);
// 0x00000069 System.Void GameScene.Logic.AddObjectComplete::OnExecute()
extern void AddObjectComplete_OnExecute_m6A7D85CDEAE4E3042B3D658386D052D5A544F609 (void);
// 0x0000006A System.Void GameScene.Logic.AddObjectComplete::.ctor()
extern void AddObjectComplete__ctor_mDD7F8445BDDEC0E773A904CB940BB7A74FA5E583 (void);
// 0x0000006B System.Void GameScene.Logic.AddRock::.ctor()
extern void AddRock__ctor_m65CE6FF13D06481DC58813B0D0480BDCD2522052 (void);
// 0x0000006C System.Void GameScene.Logic.AddRock::OnExecute()
extern void AddRock_OnExecute_m59102DF176896057CDDD85F90A0F908DBF5A7CB9 (void);
// 0x0000006D System.Void GameScene.Logic.AddRock::Activate()
extern void AddRock_Activate_m506C81956266EB1798EE7A7C23C68452186FFFB3 (void);
// 0x0000006E System.Void GameScene.Logic.AddRock::DeActivate()
extern void AddRock_DeActivate_mDCF136E029275627EFB292610331BCBACC15B56A (void);
// 0x0000006F System.Void GameScene.Logic.AddTree::.ctor()
extern void AddTree__ctor_mD0BF05F992E8640FAC3CD04C47A5CEECE688BB57 (void);
// 0x00000070 System.Void GameScene.Logic.AddTree::OnExecute()
extern void AddTree_OnExecute_m6E79CC3BF20C2B4C1258E0E7FAECE2FF22EB4AA3 (void);
// 0x00000071 System.Void GameScene.Logic.AddTree::Activate()
extern void AddTree_Activate_mB3256B51C22CF73017695B771574A832C9E4C180 (void);
// 0x00000072 System.Void GameScene.Logic.AddTree::DeActivate()
extern void AddTree_DeActivate_m00B11E36A0E469DD647A660A4B3C0FD387986789 (void);
// 0x00000073 System.Void GameScene.Logic.CheckPerimeterGenerated::OnExecute()
extern void CheckPerimeterGenerated_OnExecute_m791F1DA6BB00B2D3B33F3E061C9C5C8EEDB7BB34 (void);
// 0x00000074 System.Void GameScene.Logic.CheckPerimeterGenerated::.ctor()
extern void CheckPerimeterGenerated__ctor_m46D86C6F25004072B141E307F6EEB72F8B850095 (void);
// 0x00000075 System.Void GameScene.Logic.CheckPerimeterGenerated/<>c::.cctor()
extern void U3CU3Ec__cctor_mFDA2DB212DE1EABDA6BC0CD09EF3E4004426BCF9 (void);
// 0x00000076 System.Void GameScene.Logic.CheckPerimeterGenerated/<>c::.ctor()
extern void U3CU3Ec__ctor_m052986B92CE66527C0E56DC0AEBB7494A9B17D10 (void);
// 0x00000077 System.Boolean GameScene.Logic.CheckPerimeterGenerated/<>c::<OnExecute>b__0_0(GameScene.Model.GameObjectData)
extern void U3CU3Ec_U3COnExecuteU3Eb__0_0_mFA74F722FA08DD80ABE3C9E3861ACC79836E3CBB (void);
// 0x00000078 System.Void GameScene.Logic.DefineAvailableObjectSize::OnExecute()
extern void DefineAvailableObjectSize_OnExecute_m5722452E5D793027F33AEE3F04A6EBD1C966446B (void);
// 0x00000079 System.Boolean GameScene.Logic.DefineAvailableObjectSize::Guard()
extern void DefineAvailableObjectSize_Guard_m80A3B4323130E48A43CAB25CE37C249669FE62CA (void);
// 0x0000007A System.Void GameScene.Logic.DefineAvailableObjectSize::.ctor()
extern void DefineAvailableObjectSize__ctor_m6E2BAAA97C3A30BA4E11223025000C08E02341C7 (void);
// 0x0000007B System.Void GameScene.Logic.DefineDirectionForGeneration::OnExecute()
extern void DefineDirectionForGeneration_OnExecute_mEDC9F7D505566539A8DAB4BB133A1BD08DE19731 (void);
// 0x0000007C System.Boolean GameScene.Logic.DefineDirectionForGeneration::Guard()
extern void DefineDirectionForGeneration_Guard_m93B97140F77A8A89605EEDB63A91D7B65EAF62D9 (void);
// 0x0000007D System.Void GameScene.Logic.DefineDirectionForGeneration::.ctor()
extern void DefineDirectionForGeneration__ctor_m846372B3FBA2260D4FECA38DF5057AD1B28DDE38 (void);
// 0x0000007E System.Void GameScene.Logic.DefinePositionForGeneration::OnExecute()
extern void DefinePositionForGeneration_OnExecute_mEF3BD2A1A8C749CC64CE7B57D575244151B435BA (void);
// 0x0000007F System.Boolean GameScene.Logic.DefinePositionForGeneration::Guard()
extern void DefinePositionForGeneration_Guard_mA2C55CA03215E65C8333A1E25F014E8A81B74697 (void);
// 0x00000080 System.Void GameScene.Logic.DefinePositionForGeneration::.ctor()
extern void DefinePositionForGeneration__ctor_m906A1532A5850A99CBAEB4191839B7D74EE1B92E (void);
// 0x00000081 System.Void GameScene.Logic.GameLogic::.ctor()
extern void GameLogic__ctor_mD2FA42BEC03973480C3DB1F9B6E99BA7BFBB6582 (void);
// 0x00000082 System.Void GameScene.Logic.GameLogic::Awake()
extern void GameLogic_Awake_mC6C09DFA8CF50A93BF1DFBDDC2CD44741399D229 (void);
// 0x00000083 System.Void GameScene.Logic.GameLogic::Start()
extern void GameLogic_Start_m1410B88AC0E1B4FA97B517949EB18DFA753E7646 (void);
// 0x00000084 System.Void GameScene.Logic.GameLogic::OnDestroy()
extern void GameLogic_OnDestroy_m99D3791BD28BAFE931DB44641D267CACF152E28D (void);
// 0x00000085 System.Void GameScene.Logic.GameLogic::OnVerifyPossibleTree()
extern void GameLogic_OnVerifyPossibleTree_mCCC8B3473732C384804647BE39C9064191F42498 (void);
// 0x00000086 System.Void GameScene.Logic.GameLogic::OnVerifyPossibleRock()
extern void GameLogic_OnVerifyPossibleRock_m8F08268DD574AA6175E385E77F1E7540218E5775 (void);
// 0x00000087 System.Void GameScene.Logic.GameLogic::OnMovePossibleObject(UnityEngine.Vector3Int,UnityEngine.Vector2Int)
extern void GameLogic_OnMovePossibleObject_mA7FDFE5AE3E4294FF51C083039E07D9DBF4F1509 (void);
// 0x00000088 System.Void GameScene.Logic.GameLogic::OnShowPossibleTree(GameScene.Enums.TreeType)
extern void GameLogic_OnShowPossibleTree_m001483863720C1E1EF929506795A347495495D97 (void);
// 0x00000089 System.Void GameScene.Logic.GameLogic::OnShowPossibleRock(GameScene.Enums.RockType)
extern void GameLogic_OnShowPossibleRock_m942B19E7F5204DFC4489562C48341BD158D52718 (void);
// 0x0000008A System.Void GameScene.Logic.GameLogic::OnDelayBetweenIterationComplete(Common.Tasks.BaseTask)
extern void GameLogic_OnDelayBetweenIterationComplete_mD6484B89E3C0C63B81663A6445372965C4BDDE1F (void);
// 0x0000008B System.Void GameScene.Logic.GameLogic::OnGenerationIterationComplete(Common.Tasks.BaseTask)
extern void GameLogic_OnGenerationIterationComplete_m50C7EADE4CF7CAF8C9D8F5D5FD67DC07E6866755 (void);
// 0x0000008C System.Void GameScene.Logic.GameLogic::OnGenerationIteration()
extern void GameLogic_OnGenerationIteration_mA706BB4AC4BE17A8B10AC5402FF58BD6C163BD60 (void);
// 0x0000008D System.Void GameScene.Logic.GameLogic/<>c::.cctor()
extern void U3CU3Ec__cctor_mE26C467EDC8085EC3BDE9DA0A5756EFCCE230690 (void);
// 0x0000008E System.Void GameScene.Logic.GameLogic/<>c::.ctor()
extern void U3CU3Ec__ctor_m50B0526E3AF8A9F012C0E22452BD21DA6B0527A2 (void);
// 0x0000008F System.Boolean GameScene.Logic.GameLogic/<>c::<OnVerifyPossibleTree>b__19_0(GameScene.Model.GameObjectData)
extern void U3CU3Ec_U3COnVerifyPossibleTreeU3Eb__19_0_m3F875215589D6D2D98AD06A8890CE2903F834D2D (void);
// 0x00000090 System.Boolean GameScene.Logic.GameLogic/<>c::<OnVerifyPossibleRock>b__20_0(GameScene.Model.GameObjectData)
extern void U3CU3Ec_U3COnVerifyPossibleRockU3Eb__20_0_mF148E11EABF1780BE26BA6CB174E84D281ABBFA6 (void);
// 0x00000091 System.Void GameScene.Logic.GenerateRandomObject::.ctor()
extern void GenerateRandomObject__ctor_m6497E93DCE25FFE1B10E6107E8E49657B0164852 (void);
// 0x00000092 System.Void GameScene.Logic.GenerateRandomObject::OnExecute()
extern void GenerateRandomObject_OnExecute_mEB3CFA342723E2825307341AC162D49F6DCE7FE5 (void);
// 0x00000093 System.Void GameScene.Logic.GenerateRandomObject::Activate()
extern void GenerateRandomObject_Activate_m5449EE028447C17F6842CCF7C99D704001C43B3A (void);
// 0x00000094 System.Void GameScene.Logic.GenerateRandomObject::DeActivate()
extern void GenerateRandomObject_DeActivate_m60EC8624ECD99F354EE242ACEE673E480E3E04E2 (void);
// 0x00000095 System.Boolean GameScene.Logic.GenerateRandomObject::Guard()
extern void GenerateRandomObject_Guard_m8D0FDC3B2F3A0163B3A29CFA6BD16FD4E849970B (void);
// 0x00000096 System.Void GameScene.Logic.GenerateRandomObject/<>c::.cctor()
extern void U3CU3Ec__cctor_mB52F8E5D6C4CF3B9AEFABA1E5008D19A6724098A (void);
// 0x00000097 System.Void GameScene.Logic.GenerateRandomObject/<>c::.ctor()
extern void U3CU3Ec__ctor_mC6778BFE0353F11ED559D8EE883900D5A2CFA3C0 (void);
// 0x00000098 System.Boolean GameScene.Logic.GenerateRandomObject/<>c::<OnExecute>b__3_0(GameScene.Model.GameObjectData)
extern void U3CU3Ec_U3COnExecuteU3Eb__3_0_m6795201D994429F11655DEB8AF9832A59BBAD247 (void);
// 0x00000099 System.Boolean GameScene.Logic.GenerateRandomObject/<>c::<OnExecute>b__3_1(GameScene.Model.GameObjectData)
extern void U3CU3Ec_U3COnExecuteU3Eb__3_1_m06AB8CE3FCD2C51D0147D118DB8982616C6E763D (void);
// 0x0000009A System.Void GameScene.Logic.GenerationIteration::OnExecute()
extern void GenerationIteration_OnExecute_mD6A31F5EAC8825464A5CE08E51E58BF76FACF948 (void);
// 0x0000009B System.Void GameScene.Logic.GenerationIteration::.ctor()
extern void GenerationIteration__ctor_mD5A18E4411D185941D262C42901878278F43C0B0 (void);
// 0x0000009C System.Void GameScene.Logic.InitGameMapEmpty::OnExecute()
extern void InitGameMapEmpty_OnExecute_m634F50767C4FD92241B07D8AF9B3D446B424761F (void);
// 0x0000009D System.Void GameScene.Logic.InitGameMapEmpty::.ctor()
extern void InitGameMapEmpty__ctor_mDBAA3F062F0919313D94721BEC28657C8212B31C (void);
// 0x0000009E System.Void GameScene.Logic.InitSizeGameObjectsRelation::OnExecute()
extern void InitSizeGameObjectsRelation_OnExecute_m695B13CB0AC475478C707B8F8272E9DD94519AB2 (void);
// 0x0000009F System.Void GameScene.Logic.InitSizeGameObjectsRelation::.ctor()
extern void InitSizeGameObjectsRelation__ctor_mB710872D30F71FF27D6C86D6B7A626A5DF311278 (void);
// 0x000000A0 System.Void GameScene.Logic.ShowPossibleRock::.ctor()
extern void ShowPossibleRock__ctor_m82F637669DB376786C013DDF9956B2F3B01CB023 (void);
// 0x000000A1 System.Void GameScene.Logic.ShowPossibleRock::OnExecute()
extern void ShowPossibleRock_OnExecute_m85A22F32D3BE90DD45813DF3486F8FC8D31527C0 (void);
// 0x000000A2 System.Void GameScene.Logic.ShowPossibleRock::Activate()
extern void ShowPossibleRock_Activate_mCEB5126A50FE15B616F28BAFF8A8F233F85C9704 (void);
// 0x000000A3 System.Void GameScene.Logic.ShowPossibleRock::DeActivate()
extern void ShowPossibleRock_DeActivate_m645A007F853EBF894E7895382C7D7AF327AAD910 (void);
// 0x000000A4 System.Void GameScene.Logic.ShowPossibleTree::.ctor()
extern void ShowPossibleTree__ctor_m6369E9CD36BFE8451795E86079E97E1534C136B1 (void);
// 0x000000A5 System.Void GameScene.Logic.ShowPossibleTree::OnExecute()
extern void ShowPossibleTree_OnExecute_mA2B9760896A52EDEF70F0F7C6403D93BDD8C1018 (void);
// 0x000000A6 System.Void GameScene.Logic.ShowPossibleTree::Activate()
extern void ShowPossibleTree_Activate_mDA3EC974C16A31E5F28FB03FC3DBE58CE89232E5 (void);
// 0x000000A7 System.Void GameScene.Logic.ShowPossibleTree::DeActivate()
extern void ShowPossibleTree_DeActivate_mC9788F8D306F91160B99F5AE2F90C1A8C72D56AA (void);
// 0x000000A8 System.Void GameScene.Enums.RockTypeGuidDelegate::.ctor(System.Object,System.IntPtr)
extern void RockTypeGuidDelegate__ctor_mBC9B3C1BA34DB43861192FB72917A12FE595C228 (void);
// 0x000000A9 System.Void GameScene.Enums.RockTypeGuidDelegate::Invoke(GameScene.Enums.RockType,System.Guid)
extern void RockTypeGuidDelegate_Invoke_m4D7D6320C89BCD40815174E2CFD3E3948EDD6998 (void);
// 0x000000AA System.IAsyncResult GameScene.Enums.RockTypeGuidDelegate::BeginInvoke(GameScene.Enums.RockType,System.Guid,System.AsyncCallback,System.Object)
extern void RockTypeGuidDelegate_BeginInvoke_mD27D1B88E5A6DEE55A25F9BE764E8FDA5B8F84A5 (void);
// 0x000000AB System.Void GameScene.Enums.RockTypeGuidDelegate::EndInvoke(System.IAsyncResult)
extern void RockTypeGuidDelegate_EndInvoke_mD8CFE553B854ECF994082042E5B4C86B7A4D0A5F (void);
// 0x000000AC System.Void GameScene.Enums.TreeTypeGuidDelegate::.ctor(System.Object,System.IntPtr)
extern void TreeTypeGuidDelegate__ctor_mED9199B1A2E9ADA698C2A2FF27E52544E639D31F (void);
// 0x000000AD System.Void GameScene.Enums.TreeTypeGuidDelegate::Invoke(GameScene.Enums.TreeType,System.Guid)
extern void TreeTypeGuidDelegate_Invoke_m0AF6FFB5FF658C612B4C6402D223A54D6668D9EF (void);
// 0x000000AE System.IAsyncResult GameScene.Enums.TreeTypeGuidDelegate::BeginInvoke(GameScene.Enums.TreeType,System.Guid,System.AsyncCallback,System.Object)
extern void TreeTypeGuidDelegate_BeginInvoke_mB4F19110770B02EB85ABEC7D8E5BFB8DDC6956B9 (void);
// 0x000000AF System.Void GameScene.Enums.TreeTypeGuidDelegate::EndInvoke(System.IAsyncResult)
extern void TreeTypeGuidDelegate_EndInvoke_mC4CA8A839494FA5E642D7BB769FEA640F6B5CAFD (void);
// 0x000000B0 System.Void GameScene.Enums.RockTypeDelegate::.ctor(System.Object,System.IntPtr)
extern void RockTypeDelegate__ctor_m5553AE760EA76069028F66B4B25FCB965D78BBA5 (void);
// 0x000000B1 System.Void GameScene.Enums.RockTypeDelegate::Invoke(GameScene.Enums.RockType)
extern void RockTypeDelegate_Invoke_mE8A1950803C5A4617F699076CD65842901A40876 (void);
// 0x000000B2 System.IAsyncResult GameScene.Enums.RockTypeDelegate::BeginInvoke(GameScene.Enums.RockType,System.AsyncCallback,System.Object)
extern void RockTypeDelegate_BeginInvoke_m756B8BFEC6CEC625186195454B9DF63234F5A9DC (void);
// 0x000000B3 System.Void GameScene.Enums.RockTypeDelegate::EndInvoke(System.IAsyncResult)
extern void RockTypeDelegate_EndInvoke_mCBF34F2D3B609DA3D9CC684907DAE458D6E858A3 (void);
// 0x000000B4 System.Void GameScene.Enums.TreeTypeDelegate::.ctor(System.Object,System.IntPtr)
extern void TreeTypeDelegate__ctor_m4568BC654890C48EE908740F51BE990B8AC5DCA5 (void);
// 0x000000B5 System.Void GameScene.Enums.TreeTypeDelegate::Invoke(GameScene.Enums.TreeType)
extern void TreeTypeDelegate_Invoke_m484DB0A5FD8AB0A6F2335AF9BE0666FE0912CC43 (void);
// 0x000000B6 System.IAsyncResult GameScene.Enums.TreeTypeDelegate::BeginInvoke(GameScene.Enums.TreeType,System.AsyncCallback,System.Object)
extern void TreeTypeDelegate_BeginInvoke_m207AEA6535C58034D6EEFE907CB93237720EA6AA (void);
// 0x000000B7 System.Void GameScene.Enums.TreeTypeDelegate::EndInvoke(System.IAsyncResult)
extern void TreeTypeDelegate_EndInvoke_m149FB9958A6A890AFC9C6AAC8E32F1E813EF06AC (void);
// 0x000000B8 System.Void GameScene.Enums.Vector3IntVector2IntDelegate::.ctor(System.Object,System.IntPtr)
extern void Vector3IntVector2IntDelegate__ctor_mC783EA6E7EF6F7B57648CF555E9FC67745226DC9 (void);
// 0x000000B9 System.Void GameScene.Enums.Vector3IntVector2IntDelegate::Invoke(UnityEngine.Vector3Int,UnityEngine.Vector2Int)
extern void Vector3IntVector2IntDelegate_Invoke_mB6D04F9A660265DC656B712F576363175A9FA30E (void);
// 0x000000BA System.IAsyncResult GameScene.Enums.Vector3IntVector2IntDelegate::BeginInvoke(UnityEngine.Vector3Int,UnityEngine.Vector2Int,System.AsyncCallback,System.Object)
extern void Vector3IntVector2IntDelegate_BeginInvoke_m9BC8B1BFBEF78257E5DBC7AA0FB0E312CF65BFAD (void);
// 0x000000BB System.Void GameScene.Enums.Vector3IntVector2IntDelegate::EndInvoke(System.IAsyncResult)
extern void Vector3IntVector2IntDelegate_EndInvoke_m196891133FD447634111054EA8821E972DCB2568 (void);
// 0x000000BC System.Void GameScene.Enums.RockViewDelegate::.ctor(System.Object,System.IntPtr)
extern void RockViewDelegate__ctor_mCAD28039F45E633B125C2B02345E3976BAEA7245 (void);
// 0x000000BD System.Void GameScene.Enums.RockViewDelegate::Invoke(GameScene.View.RockView)
extern void RockViewDelegate_Invoke_m2ADEFED25721C6694839442B4C48BD65D8E8A6C0 (void);
// 0x000000BE System.IAsyncResult GameScene.Enums.RockViewDelegate::BeginInvoke(GameScene.View.RockView,System.AsyncCallback,System.Object)
extern void RockViewDelegate_BeginInvoke_m99D9AE8F006DCBEAEFBF6EDDD9E8568ABEDAEB68 (void);
// 0x000000BF System.Void GameScene.Enums.RockViewDelegate::EndInvoke(System.IAsyncResult)
extern void RockViewDelegate_EndInvoke_mD2E1DD3BCCAB3D2A2D10FE11D3EF61C26121220B (void);
// 0x000000C0 System.Void GameScene.Enums.TreeViewDelegate::.ctor(System.Object,System.IntPtr)
extern void TreeViewDelegate__ctor_m2DF3D3E414E6994894FDFBE746AEEB2698F33E4E (void);
// 0x000000C1 System.Void GameScene.Enums.TreeViewDelegate::Invoke(GameScene.View.TreeView)
extern void TreeViewDelegate_Invoke_m381A96A4F32924955CBA228D6EBDFB28DBC10218 (void);
// 0x000000C2 System.IAsyncResult GameScene.Enums.TreeViewDelegate::BeginInvoke(GameScene.View.TreeView,System.AsyncCallback,System.Object)
extern void TreeViewDelegate_BeginInvoke_mEFA3FB3C2640E5F893F85B267581F38ECE04792C (void);
// 0x000000C3 System.Void GameScene.Enums.TreeViewDelegate::EndInvoke(System.IAsyncResult)
extern void TreeViewDelegate_EndInvoke_mD689120A5845DA0591A3C0ED1F6520AA3FE0ED00 (void);
// 0x000000C4 System.Void GameScene.Config.GameCameraConfig::.cctor()
extern void GameCameraConfig__cctor_m705146193CC5B80951E6E6F2FC3E87DB6C22183B (void);
// 0x000000C5 System.Void GameScene.Config.GameMapConfig::.cctor()
extern void GameMapConfig__cctor_mB798A1C5B908BDC306B0621DD0B2A5128114A9C4 (void);
// 0x000000C6 System.Void GameScene.Config.SizeConfig::.cctor()
extern void SizeConfig__cctor_mD0DB491428EF534782F0CE1BF301DB07E86F6B5F (void);
// 0x000000C7 System.Boolean System.ObjectExtensions::IsPrimitive(System.Type)
extern void ObjectExtensions_IsPrimitive_mE3413D220E53574C90F68C8C789487FB8E1340F3 (void);
// 0x000000C8 System.Object System.ObjectExtensions::Copy(System.Object)
extern void ObjectExtensions_Copy_m49548073C536D88E334ED506ECED6833B5F42EF5 (void);
// 0x000000C9 System.Object System.ObjectExtensions::InternalCopy(System.Object,System.Collections.Generic.IDictionary`2<System.Object,System.Object>)
extern void ObjectExtensions_InternalCopy_m231ED0B451B4803AA35877ECA937C5ABE7896E6E (void);
// 0x000000CA System.Void System.ObjectExtensions::RecursiveCopyBaseTypePrivateFields(System.Object,System.Collections.Generic.IDictionary`2<System.Object,System.Object>,System.Object,System.Type)
extern void ObjectExtensions_RecursiveCopyBaseTypePrivateFields_m01D153B957C26F29A83EC530F783CABC08F69637 (void);
// 0x000000CB System.Void System.ObjectExtensions::CopyFields(System.Object,System.Collections.Generic.IDictionary`2<System.Object,System.Object>,System.Object,System.Type,System.Reflection.BindingFlags,System.Func`2<System.Reflection.FieldInfo,System.Boolean>)
extern void ObjectExtensions_CopyFields_m8AEBB8646E9DC2AB83B37AB277E9CFC4A35C408E (void);
// 0x000000CC T System.ObjectExtensions::Copy(T)
// 0x000000CD System.Void System.ObjectExtensions::.cctor()
extern void ObjectExtensions__cctor_m7D2B24D9EE3450F3F6DB06204F47EF593D96D3C8 (void);
// 0x000000CE System.Void System.ObjectExtensions/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mD5542E6AB3FA5C38F2C8AE42F559A9AE81A3C48F (void);
// 0x000000CF System.Void System.ObjectExtensions/<>c__DisplayClass3_1::.ctor()
extern void U3CU3Ec__DisplayClass3_1__ctor_mEA88BBA45FC2CBA1BC27CDF235B3E9E7E418A5EF (void);
// 0x000000D0 System.Void System.ObjectExtensions/<>c__DisplayClass3_1::<InternalCopy>b__0(System.Array,System.Int32[])
extern void U3CU3Ec__DisplayClass3_1_U3CInternalCopyU3Eb__0_mFFAF34C84F89FFD93615734E815C13AA2B771D82 (void);
// 0x000000D1 System.Void System.ObjectExtensions/<>c::.cctor()
extern void U3CU3Ec__cctor_mC45DE533C026BAC3A6A04C8336D94660821ECB5A (void);
// 0x000000D2 System.Void System.ObjectExtensions/<>c::.ctor()
extern void U3CU3Ec__ctor_m6D2B9138E5D9D034277A71C8A73271186005E11F (void);
// 0x000000D3 System.Boolean System.ObjectExtensions/<>c::<RecursiveCopyBaseTypePrivateFields>b__4_0(System.Reflection.FieldInfo)
extern void U3CU3Ec_U3CRecursiveCopyBaseTypePrivateFieldsU3Eb__4_0_mB047F81F9F692FA41781AE64EBFAEFF05A006609 (void);
// 0x000000D4 System.Boolean System.ReferenceEqualityComparer::Equals(System.Object,System.Object)
extern void ReferenceEqualityComparer_Equals_m830A6744426166FEC917035E62E2B8911BFD84B5 (void);
// 0x000000D5 System.Int32 System.ReferenceEqualityComparer::GetHashCode(System.Object)
extern void ReferenceEqualityComparer_GetHashCode_mD52702F2722A2955E1FBDAEFB0B1F556B9C176EC (void);
// 0x000000D6 System.Void System.ReferenceEqualityComparer::.ctor()
extern void ReferenceEqualityComparer__ctor_m1B21137BC03F0048695FF25B4D6D2CF34ED495AA (void);
// 0x000000D7 System.Void System.ArrayExtensions.ArrayExtensions::ForEach(System.Array,System.Action`2<System.Array,System.Int32[]>)
extern void ArrayExtensions_ForEach_m28922D9F29A0093D1EE37A70AEF1AC1F485E59BA (void);
// 0x000000D8 System.Void System.ArrayExtensions.ArrayTraverse::.ctor(System.Array)
extern void ArrayTraverse__ctor_mCFDE8139EB5CD94042B84E151802FAF8D3D682A3 (void);
// 0x000000D9 System.Boolean System.ArrayExtensions.ArrayTraverse::Step()
extern void ArrayTraverse_Step_m56CF3C645443EDB9AC909E45F3792B1B0B11475D (void);
// 0x000000DA System.Void Common.View.CanvasAutoResize::OnEnable()
extern void CanvasAutoResize_OnEnable_m3AD0334FDD4331B71E250BA51553CD3A4A9A5019 (void);
// 0x000000DB System.Void Common.View.CanvasAutoResize::.ctor()
extern void CanvasAutoResize__ctor_mDD19FB2A76E4077833CB0AC6F775517D23207528 (void);
// 0x000000DC System.Void Common.View.FadeScreenView::.ctor()
extern void FadeScreenView__ctor_m2D606BDC2D4E1760BEC48194C12E32191E08D1E8 (void);
// 0x000000DD System.Void Common.View.FadeScreenView::Awake()
extern void FadeScreenView_Awake_mECF628CDF7464AFDCAF18E86D9DDA11E721B728C (void);
// 0x000000DE System.Void Common.View.FadeScreenView::OnDestroy()
extern void FadeScreenView_OnDestroy_m564CCFBD770C638C40BA9E8F9F9E46A3852403CF (void);
// 0x000000DF System.Void Common.View.FadeScreenView::ClearTween()
extern void FadeScreenView_ClearTween_m96E2FA183C419BA33055C2FDEEF2C1CC4C9299C6 (void);
// 0x000000E0 System.Void Common.View.FadeScreenView::OnFadeScreen()
extern void FadeScreenView_OnFadeScreen_m25D5FD52A7FB2B89A00F71189F0469B6E435D170 (void);
// 0x000000E1 System.Void Common.View.FadeScreenView::OnFadeOutScreen()
extern void FadeScreenView_OnFadeOutScreen_m516CCD0E883397721E5C7AF3EE47036E042A8550 (void);
// 0x000000E2 System.Void Common.View.FadeScreenView::<OnFadeOutScreen>b__10_0()
extern void FadeScreenView_U3COnFadeOutScreenU3Eb__10_0_m5C0EF6DF8886C3BC32FBFFDD97F3D718A17727A6 (void);
// 0x000000E3 System.Void Common.View.FadeScreenView/<>c::.cctor()
extern void U3CU3Ec__cctor_mA76D2A86A164B763014083751F3256F0F36DFF5A (void);
// 0x000000E4 System.Void Common.View.FadeScreenView/<>c::.ctor()
extern void U3CU3Ec__ctor_m130045401B8D8A1538092FDE11E669791D237A3C (void);
// 0x000000E5 System.Void Common.View.FadeScreenView/<>c::<OnFadeScreen>b__9_0()
extern void U3CU3Ec_U3COnFadeScreenU3Eb__9_0_m56FE9BCF514C4C5BE73179484C588EDC4E38851A (void);
// 0x000000E6 System.Void Common.View.SafeArea::Awake()
extern void SafeArea_Awake_m5F9C4BAB44820BA7C2CEE88799894AA9676B4031 (void);
// 0x000000E7 System.Void Common.View.SafeArea::.ctor()
extern void SafeArea__ctor_m7685F4CA5E5037198D49E9AF2FEA5F6F8AF809E7 (void);
// 0x000000E8 System.Void Common.View.UserInterfaceLocker::.ctor()
extern void UserInterfaceLocker__ctor_m7DB40D8EC1151A866D059177550C007CBECB92B2 (void);
// 0x000000E9 System.Void Common.View.UserInterfaceLocker::OnEnable()
extern void UserInterfaceLocker_OnEnable_mABF1ACFCDB8ABEEAC3C46E4E61FE63EE0A80035F (void);
// 0x000000EA System.Void Common.View.UserInterfaceLocker::OnDisable()
extern void UserInterfaceLocker_OnDisable_m2260099229194452EB16E5CBCC8C9A700C76AF53 (void);
// 0x000000EB System.Void Common.View.UserInterfaceLocker::OnUnLock()
extern void UserInterfaceLocker_OnUnLock_m781B9DB71241A1E48EBC375CF5E83BA0E189A984 (void);
// 0x000000EC System.Void Common.View.UserInterfaceLocker::OnLock()
extern void UserInterfaceLocker_OnLock_mF8CDFD4131D843C6063000FD6A8C1A06391C64EC (void);
// 0x000000ED System.Boolean Common.Tasks.BaseTask::get_inProgress()
extern void BaseTask_get_inProgress_mDA8BF0F90B97AC0165A19FF0EAC9412D28C06D5B (void);
// 0x000000EE System.Void Common.Tasks.BaseTask::set_inProgress(System.Boolean)
extern void BaseTask_set_inProgress_m8A9CB52395583D9689BE39C7B51E5AD67F5AD36D (void);
// 0x000000EF System.Void Common.Tasks.BaseTask::ForceModeOn()
extern void BaseTask_ForceModeOn_mC79E2EAA70E4183832344451A814D82A43792C2E (void);
// 0x000000F0 System.Void Common.Tasks.BaseTask::Activate()
extern void BaseTask_Activate_mF1A5137EAAB72F844BB6558D02DB473C4BE63678 (void);
// 0x000000F1 System.Void Common.Tasks.BaseTask::DeActivate()
extern void BaseTask_DeActivate_m491554BBCC754389E193EEA6086989EC4B0FEF4D (void);
// 0x000000F2 System.Void Common.Tasks.BaseTask::ForceComplete()
extern void BaseTask_ForceComplete_m01D5FEB2BA5BB63066FFC10F88C48410EA8A8AEF (void);
// 0x000000F3 System.Void Common.Tasks.BaseTask::Execute()
extern void BaseTask_Execute_mC071A3C21E2FD567CF9A2602B3C70CE9517203DC (void);
// 0x000000F4 System.Boolean Common.Tasks.BaseTask::Guard()
extern void BaseTask_Guard_m0BA8470E3BB673D9F51CCC2A115C6D3D1D512DDE (void);
// 0x000000F5 System.Void Common.Tasks.BaseTask::OnGuard()
extern void BaseTask_OnGuard_mD7C45815D9BDF88CC6454D0333742B9D8A3F57F0 (void);
// 0x000000F6 System.Void Common.Tasks.BaseTask::Complete()
extern void BaseTask_Complete_m9FF8D6F02082567CF9D6D36B15D8067D44C1F015 (void);
// 0x000000F7 System.Void Common.Tasks.BaseTask::Terminate()
extern void BaseTask_Terminate_m90A563088C350AD2E0A5BDA273F05B2CC1968BD2 (void);
// 0x000000F8 System.Void Common.Tasks.BaseTask::OnTerminate()
extern void BaseTask_OnTerminate_m7E816CF9836573AEA460781F23C85EE740D10AA9 (void);
// 0x000000F9 System.Void Common.Tasks.BaseTask::OnExecute()
extern void BaseTask_OnExecute_m2EE62F18BA83F88CD06D0087BD4229628EC127B4 (void);
// 0x000000FA System.Void Common.Tasks.BaseTask::OnForceExecute()
extern void BaseTask_OnForceExecute_m71559EF7834F7B8DF388A92086DBBE84882E43EE (void);
// 0x000000FB System.Void Common.Tasks.BaseTask::.ctor()
extern void BaseTask__ctor_mCB916DF503E63F75CB330C095E34ABE4D48C7532 (void);
// 0x000000FC System.Void Common.Tasks.DelayTask::OnExecute()
extern void DelayTask_OnExecute_mA7397B8AAF3752E96B02108266DA02F4B55396D7 (void);
// 0x000000FD System.Void Common.Tasks.DelayTask::DelayComplete()
extern void DelayTask_DelayComplete_m18CF5B94D88F6DB5F1353255CC03FE4AAE195F05 (void);
// 0x000000FE System.Void Common.Tasks.DelayTask::Complete()
extern void DelayTask_Complete_m39FFEBB63A9801BE7563CA78F269D2114FDC63BE (void);
// 0x000000FF System.Void Common.Tasks.DelayTask::OnTerminate()
extern void DelayTask_OnTerminate_m1BA233109CAA3321E1224C3B2868C5E088E399FC (void);
// 0x00000100 System.Void Common.Tasks.DelayTask::ClearDelay()
extern void DelayTask_ClearDelay_mB4C9F85F357C14BFD583993D14E5B84B78645682 (void);
// 0x00000101 System.Void Common.Tasks.DelayTask::OnDestroy()
extern void DelayTask_OnDestroy_m95ADEA2CE295986A8BA16730ABFD6D75663FDC52 (void);
// 0x00000102 System.Void Common.Tasks.DelayTask::.ctor()
extern void DelayTask__ctor_m56543B6730DBB9113E4733E1F8782CF7259C09B1 (void);
// 0x00000103 System.Void Common.Tasks.LoopSequence::Complete()
extern void LoopSequence_Complete_m95E0767EC773F310ADF59E0451E4F7C7AC90B298 (void);
// 0x00000104 System.Void Common.Tasks.LoopSequence::.ctor()
extern void LoopSequence__ctor_m74ABFDDB0EC942A495E5A41959A7C7CB7D8DE672 (void);
// 0x00000105 System.Void Common.Tasks.ParallelTask::Awake()
extern void ParallelTask_Awake_m21CFF8CD3713DD7B79FAE9977DB13ADF05FAA350 (void);
// 0x00000106 System.Void Common.Tasks.ParallelTask::DeActivate()
extern void ParallelTask_DeActivate_m49C2DB3625F90638991A369DC45ACAFF0B3F91EA (void);
// 0x00000107 System.Void Common.Tasks.ParallelTask::OnChildTaskComplete(Common.Tasks.BaseTask)
extern void ParallelTask_OnChildTaskComplete_m8C6821FE302FC94E9B12F42F78E4292951FE5560 (void);
// 0x00000108 System.Void Common.Tasks.ParallelTask::OnChildTaskGuard(Common.Tasks.BaseTask)
extern void ParallelTask_OnChildTaskGuard_m93853BA5B44B13F75E826561BF6A1684419D7407 (void);
// 0x00000109 System.Void Common.Tasks.ParallelTask::OnForceExecute()
extern void ParallelTask_OnForceExecute_m8F9D57B84ABD1554BF29162A93507EB8FA1185C0 (void);
// 0x0000010A System.Void Common.Tasks.ParallelTask::OnExecute()
extern void ParallelTask_OnExecute_mBCE45C10F3F215352C0A22B7B0C238D292C37CB3 (void);
// 0x0000010B System.Void Common.Tasks.ParallelTask::ForceModeOn()
extern void ParallelTask_ForceModeOn_mB17B343A45DD7C387170C294A75CEF0ED2BF7FEA (void);
// 0x0000010C System.Void Common.Tasks.ParallelTask::OnTerminate()
extern void ParallelTask_OnTerminate_m399A03BEF1E2FCBCF7DF2CC63A319BA13F208A49 (void);
// 0x0000010D System.Void Common.Tasks.ParallelTask::.ctor()
extern void ParallelTask__ctor_mC6AA50D04C1ED2DF093C43829C5074709C8B79C8 (void);
// 0x0000010E System.Void Common.Tasks.SequenceTask::Awake()
extern void SequenceTask_Awake_mB106FB2C0B971B313A8578F7AB42DFDC59C1D48C (void);
// 0x0000010F System.Void Common.Tasks.SequenceTask::DeActivate()
extern void SequenceTask_DeActivate_mDFE9F383BD44B8AD8ADC5932B61E29EEDC6808FC (void);
// 0x00000110 System.Void Common.Tasks.SequenceTask::OnChildTaskComplete(Common.Tasks.BaseTask)
extern void SequenceTask_OnChildTaskComplete_m726189F465C21A6198F7BBD6D95B0DC518137982 (void);
// 0x00000111 System.Void Common.Tasks.SequenceTask::OnChildTaskGuard(Common.Tasks.BaseTask)
extern void SequenceTask_OnChildTaskGuard_m048D7C2EC39559FB946303E9961B087888471B30 (void);
// 0x00000112 System.Void Common.Tasks.SequenceTask::OnForceExecute()
extern void SequenceTask_OnForceExecute_mFACE297E774DE005634AC9A1958CDBB25F0D5E28 (void);
// 0x00000113 System.Void Common.Tasks.SequenceTask::OnExecute()
extern void SequenceTask_OnExecute_m0CFA3437DF12189626CA3374A140CEFB7F7A0EDE (void);
// 0x00000114 System.Void Common.Tasks.SequenceTask::NextTask()
extern void SequenceTask_NextTask_mED2A3E582A9E0EE0C7219A28EB155DA66E936D80 (void);
// 0x00000115 System.Void Common.Tasks.SequenceTask::ForceModeOn()
extern void SequenceTask_ForceModeOn_m28F8CAC1271503FD6D7C2DD09AF54926ED40C3FC (void);
// 0x00000116 System.Void Common.Tasks.SequenceTask::OnTerminate()
extern void SequenceTask_OnTerminate_m3A45A40EA912222BE7EE43D5F3B852F7278B28FA (void);
// 0x00000117 System.Void Common.Tasks.SequenceTask::.ctor()
extern void SequenceTask__ctor_m900ABCEDE774E883351847CAE97E58E87071F070 (void);
// 0x00000118 Common.Singleton.MainModel Common.Singleton.MainModel::get_Instance()
extern void MainModel_get_Instance_m9CA908B2B90D7AA5EFE64F9D138D2E05A03B98F4 (void);
// 0x00000119 GameScene.Model.GameData Common.Singleton.MainModel::get_GameData()
extern void MainModel_get_GameData_mDEDAECA9AF12EF27CFF9962E582161880E1D6A5F (void);
// 0x0000011A System.Void Common.Singleton.MainModel::set_GameData(GameScene.Model.GameData)
extern void MainModel_set_GameData_mA943CF7D6AEE4B0D5684106FD4C90E3356B852FF (void);
// 0x0000011B System.Boolean Common.Singleton.MainModel::IsNull()
extern void MainModel_IsNull_m84315D41DC57A8FF87A5C713F428062DCB2DB301 (void);
// 0x0000011C System.Void Common.Singleton.MainModel::Init()
extern void MainModel_Init_m33292F75E1F5BF26B12CA915955FABD7E666DD22 (void);
// 0x0000011D System.Void Common.Singleton.MainModel::.ctor()
extern void MainModel__ctor_m0D67886949C03C3CC8642AC492A50C39A72BFA3B (void);
// 0x0000011E Common.Singleton.Observer Common.Singleton.Observer::get_instance()
extern void Observer_get_instance_m7C1960F33B8EDD3279901CEF27B68360CB549701 (void);
// 0x0000011F System.Void Common.Singleton.Observer::Init()
extern void Observer_Init_m703BA2F07719623E769FD7F66006561B7DBBF974 (void);
// 0x00000120 System.Boolean Common.Singleton.Observer::IsNull()
extern void Observer_IsNull_m2AB010437EBBFC983B0DBD0E5923B887CD8DCB8C (void);
// 0x00000121 System.Void Common.Singleton.Observer::AddListener(System.String,System.Delegate)
extern void Observer_AddListener_m42738DFF2FF70F0F4B02E92EF67CC35957F6D09F (void);
// 0x00000122 System.Void Common.Singleton.Observer::RemoveListener(System.String,System.Delegate)
extern void Observer_RemoveListener_m949C560C91E701AD2DD5FFF2D085C25D790BFE40 (void);
// 0x00000123 System.Void Common.Singleton.Observer::Emit(System.String,System.Object[])
extern void Observer_Emit_m81B123C91DF6729DD7B546693B57E344FB5E99F2 (void);
// 0x00000124 System.Void Common.Singleton.Observer::.ctor()
extern void Observer__ctor_mDAA046FC2B9FDA6036DF5DA00BFFCCC9A4435CBD (void);
// 0x00000125 System.Void Common.Logic.FadeOutScreen::.ctor()
extern void FadeOutScreen__ctor_mAB57C61FF756BC83007E28465556A64FAC6211D3 (void);
// 0x00000126 System.Void Common.Logic.FadeOutScreen::OnExecute()
extern void FadeOutScreen_OnExecute_m8A35E67ADF0D1584A7715698F72A400ED9B464AC (void);
// 0x00000127 System.Void Common.Logic.FadeOutScreen::Activate()
extern void FadeOutScreen_Activate_m0E301803199084BE7B3F2A2AA06BDF7E8518A8FC (void);
// 0x00000128 System.Void Common.Logic.FadeOutScreen::DeActivate()
extern void FadeOutScreen_DeActivate_m62F4A8251606434A7D90BF73E157BD9367A7A11D (void);
// 0x00000129 System.Void Common.Logic.FadeScreen::.ctor()
extern void FadeScreen__ctor_m1D87124A13454BEC77B14C1657BE22C535661C0D (void);
// 0x0000012A System.Void Common.Logic.FadeScreen::OnExecute()
extern void FadeScreen_OnExecute_mCFFA10DD2336A4AD451F4B76DE013E8C8FAF8EF8 (void);
// 0x0000012B System.Void Common.Logic.FadeScreen::Activate()
extern void FadeScreen_Activate_mA200730C878D687126DBD45497734ADB0A2E4F4D (void);
// 0x0000012C System.Void Common.Logic.FadeScreen::DeActivate()
extern void FadeScreen_DeActivate_mB9CEAFCC75E3F226EEDAD238A6DE30F4B2F50ECB (void);
// 0x0000012D System.Void Common.Logic.LockUserInterface::OnExecute()
extern void LockUserInterface_OnExecute_m15F3318F745C13204B5116C25098F907C6419B0C (void);
// 0x0000012E System.Void Common.Logic.LockUserInterface::.ctor()
extern void LockUserInterface__ctor_m6A53A6A2DE617ECB1BBFB57C683FDB4F37AB50F4 (void);
// 0x0000012F System.Void Common.Logic.UnLockUserInterface::OnExecute()
extern void UnLockUserInterface_OnExecute_m6BDC0A0DC9B2C929B2B4AA8294999E27E0AB4777 (void);
// 0x00000130 System.Void Common.Logic.UnLockUserInterface::.ctor()
extern void UnLockUserInterface__ctor_m41DE93D23E509A4AAD57BCC19F24675E1BFF3342 (void);
// 0x00000131 System.Void Common.Enums.SimpleDelegate::.ctor(System.Object,System.IntPtr)
extern void SimpleDelegate__ctor_m2E868FB5069AFD65CE57DBC517B2FDCA8590B0F1 (void);
// 0x00000132 System.Void Common.Enums.SimpleDelegate::Invoke()
extern void SimpleDelegate_Invoke_m614A63B4251A1A0C7937E13BFD96E726783B8D4F (void);
// 0x00000133 System.IAsyncResult Common.Enums.SimpleDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void SimpleDelegate_BeginInvoke_m9D726B2DE38240F3503F74F753F6FAA85724B766 (void);
// 0x00000134 System.Void Common.Enums.SimpleDelegate::EndInvoke(System.IAsyncResult)
extern void SimpleDelegate_EndInvoke_mB3D5A911A068606E57755482A66B517A276CC11C (void);
// 0x00000135 System.Void Common.Enums.ReturnIntSimpleDelegate::.ctor(System.Object,System.IntPtr)
extern void ReturnIntSimpleDelegate__ctor_mD21E3CC47176D55226CF839C9AFA10E4C0C0A6E3 (void);
// 0x00000136 System.Int32 Common.Enums.ReturnIntSimpleDelegate::Invoke()
extern void ReturnIntSimpleDelegate_Invoke_m4341C9FCA402B25C3FAF0F684F12A1C621D1C072 (void);
// 0x00000137 System.IAsyncResult Common.Enums.ReturnIntSimpleDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void ReturnIntSimpleDelegate_BeginInvoke_m75871ED5CDC17D2F8760DA20FF50C7F646EC27B3 (void);
// 0x00000138 System.Int32 Common.Enums.ReturnIntSimpleDelegate::EndInvoke(System.IAsyncResult)
extern void ReturnIntSimpleDelegate_EndInvoke_mA7C2B72F905287F2AB680C7E8F50631400734440 (void);
// 0x00000139 System.Void Common.Enums.TaskDelegate::.ctor(System.Object,System.IntPtr)
extern void TaskDelegate__ctor_mB888EF198554486F35A5E40772A6A849B4502228 (void);
// 0x0000013A System.Void Common.Enums.TaskDelegate::Invoke(Common.Tasks.BaseTask)
extern void TaskDelegate_Invoke_m2342944B9977B07715EF94A0BFA34CBAE7B02AA0 (void);
// 0x0000013B System.IAsyncResult Common.Enums.TaskDelegate::BeginInvoke(Common.Tasks.BaseTask,System.AsyncCallback,System.Object)
extern void TaskDelegate_BeginInvoke_m4D8804ABA093F7E0DE92132428B8616930E295A8 (void);
// 0x0000013C System.Void Common.Enums.TaskDelegate::EndInvoke(System.IAsyncResult)
extern void TaskDelegate_EndInvoke_m7C789AE7A416592A29D3684D393912261892A264 (void);
// 0x0000013D System.Void Common.Enums.BoolDelegate::.ctor(System.Object,System.IntPtr)
extern void BoolDelegate__ctor_mB9310402E1B4D3F8379ACEE4185C4BE7BE0EE692 (void);
// 0x0000013E System.Void Common.Enums.BoolDelegate::Invoke(System.Boolean)
extern void BoolDelegate_Invoke_mC52BA7DD5BC6EBBA7F5AC9B5FB0EC353DFF66C6F (void);
// 0x0000013F System.IAsyncResult Common.Enums.BoolDelegate::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void BoolDelegate_BeginInvoke_mBFAD186494E93C8F42546F0E688160E06D085C78 (void);
// 0x00000140 System.Void Common.Enums.BoolDelegate::EndInvoke(System.IAsyncResult)
extern void BoolDelegate_EndInvoke_mE72D712D3DF42FDA97808F8D044FF02A74C3B083 (void);
// 0x00000141 System.Void Common.Enums.IntDelegate::.ctor(System.Object,System.IntPtr)
extern void IntDelegate__ctor_m56FD9C29DB5F90C4C4790B073796BD8C2FD1292A (void);
// 0x00000142 System.Void Common.Enums.IntDelegate::Invoke(System.Int32)
extern void IntDelegate_Invoke_mDA997F958C361CCD1C74C09DF153D00D0D5F1CBA (void);
// 0x00000143 System.IAsyncResult Common.Enums.IntDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void IntDelegate_BeginInvoke_mABD76ADAAF7545F0C086E07068DB0446B1315808 (void);
// 0x00000144 System.Void Common.Enums.IntDelegate::EndInvoke(System.IAsyncResult)
extern void IntDelegate_EndInvoke_m963FC51DDD5522A7F1168574E1D4C9D2E7889CB5 (void);
// 0x00000145 System.Void Common.Enums.IntIntDelegate::.ctor(System.Object,System.IntPtr)
extern void IntIntDelegate__ctor_m46CA6ECA7E1BC80FE1394C10DA409423CEE59CCF (void);
// 0x00000146 System.Void Common.Enums.IntIntDelegate::Invoke(System.Int32,System.Int32)
extern void IntIntDelegate_Invoke_m52F7E8C6246FF3ACFB0948F152C3DF7E6BC79576 (void);
// 0x00000147 System.IAsyncResult Common.Enums.IntIntDelegate::BeginInvoke(System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void IntIntDelegate_BeginInvoke_mFA6A62C5FEC4A86F4EE8C6EA33579343977529A0 (void);
// 0x00000148 System.Void Common.Enums.IntIntDelegate::EndInvoke(System.IAsyncResult)
extern void IntIntDelegate_EndInvoke_m43B20B298828F13553BD7E07D09B09599C3CE880 (void);
// 0x00000149 System.Void Common.Enums.FloatDelegate::.ctor(System.Object,System.IntPtr)
extern void FloatDelegate__ctor_m08E8FE9B6517FC2A72F691B1F534D6A431948376 (void);
// 0x0000014A System.Void Common.Enums.FloatDelegate::Invoke(System.Single)
extern void FloatDelegate_Invoke_mBC917CFC7D1E123C8FDE74CB13680BF285C3A4FD (void);
// 0x0000014B System.IAsyncResult Common.Enums.FloatDelegate::BeginInvoke(System.Single,System.AsyncCallback,System.Object)
extern void FloatDelegate_BeginInvoke_mB1288165236956665F157980EE2C5A127E5EAF9F (void);
// 0x0000014C System.Void Common.Enums.FloatDelegate::EndInvoke(System.IAsyncResult)
extern void FloatDelegate_EndInvoke_mB2B7E192660642B03453C786385FED321AA21C5A (void);
// 0x0000014D System.Void Common.Enums.StringDelegate::.ctor(System.Object,System.IntPtr)
extern void StringDelegate__ctor_m908786AB82F31EA91BD74E529D74F4E0D0568F10 (void);
// 0x0000014E System.Void Common.Enums.StringDelegate::Invoke(System.String)
extern void StringDelegate_Invoke_m3F5D3C51C57214F9BE2B6F25B997CFBFC5D822A2 (void);
// 0x0000014F System.IAsyncResult Common.Enums.StringDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void StringDelegate_BeginInvoke_mC545894BE023A46DD56D8A525ECFD13E69CD5929 (void);
// 0x00000150 System.Void Common.Enums.StringDelegate::EndInvoke(System.IAsyncResult)
extern void StringDelegate_EndInvoke_mAD4E31A175D8141C29EA5CA5345C1FF2DD41CD1F (void);
// 0x00000151 System.Void Common.Enums.Vector2IntDelegate::.ctor(System.Object,System.IntPtr)
extern void Vector2IntDelegate__ctor_m6FFDA70806B0CA3ABE899C9E76FEE91E91602FD9 (void);
// 0x00000152 System.Void Common.Enums.Vector2IntDelegate::Invoke(UnityEngine.Vector2Int)
extern void Vector2IntDelegate_Invoke_mBFD7148B6FE83FFB6AE2AD9B5BE39CEC7F547E74 (void);
// 0x00000153 System.IAsyncResult Common.Enums.Vector2IntDelegate::BeginInvoke(UnityEngine.Vector2Int,System.AsyncCallback,System.Object)
extern void Vector2IntDelegate_BeginInvoke_m9F5E25F263EC0DACB5A465CC5899412003EE9E40 (void);
// 0x00000154 System.Void Common.Enums.Vector2IntDelegate::EndInvoke(System.IAsyncResult)
extern void Vector2IntDelegate_EndInvoke_mD90467D5F822A6DE7A814B23B52470BE1D55369B (void);
// 0x00000155 System.Void Common.Enums.Vector3Delegate::.ctor(System.Object,System.IntPtr)
extern void Vector3Delegate__ctor_m73A537DCF00AC53073D1711EF3A8CFD31165E17C (void);
// 0x00000156 System.Void Common.Enums.Vector3Delegate::Invoke(UnityEngine.Vector3)
extern void Vector3Delegate_Invoke_mBEC64925D910E779C406887207F444B1B53AD249 (void);
// 0x00000157 System.IAsyncResult Common.Enums.Vector3Delegate::BeginInvoke(UnityEngine.Vector3,System.AsyncCallback,System.Object)
extern void Vector3Delegate_BeginInvoke_m2F3B39BC71CDAAA15F1A6FC50DD173719FF93092 (void);
// 0x00000158 System.Void Common.Enums.Vector3Delegate::EndInvoke(System.IAsyncResult)
extern void Vector3Delegate_EndInvoke_mE006EE61AAB89B029080E24C6672637F8E12D7E6 (void);
static Il2CppMethodPointer s_methodPointers[344] = 
{
	AsyncSceneLoader_Start_m53F804786571201F7BDE310B292D22BF947B29A3,
	AsyncSceneLoader_Update_m095776B9718DA0D2C2730A7FF4B0C1E16EBA1761,
	AsyncSceneLoader__ctor_mDB001A538CE6A6FCA45D036F886C1E982716A5A9,
	AddControlSegment__ctor_m0167FE6EB9C17C9F71C2229E27F3ADE9EC51361D,
	AddControlSegment_OnPerimeterGenerationComplete_m18E9C32A60E3668B15F84CEB6AD45997B22981BF,
	AddControlSegment_Awake_mADA80BE9079670601EAE9D21C2322048E6B93996,
	AddControlSegment_OnDestroy_mD9391CFC172FF52243EE64A651DB1B0E4A31AC81,
	AddControlSegment_OnGetTreeInfo_m878054E5A5E7DCF0FE70D3BCB8FF8B0AEF8141CA,
	AddControlSegment_OnGetRockInfo_mAB54DB33B99FE6A8F3F41D31EB6569416C859B66,
	AddControlSegment_OnVerifyFailed_m75A02685C47EB564E1811585A589B704D884F5D8,
	AddControlSegment_ResetInfoText_m3F93E3417F9959F8D9A595F0ED322F276DF1B43D,
	AddControlSegment_OnAddObjectComplete_m16C2CE810E3431F8B5508D11A1564032825385D2,
	AddControlSegment_OnShowPossibleTree_m926DF99B7EB184908975F1011071427115FCE237,
	AddControlSegment_OnShowPossibleRock_m2F07B0A2172CE1B707E9AD75F1CC2990795C9F16,
	AddControlSegment_Accept_m2C023FB6C1488B0AE618239FE8686306FA5A3F3A,
	AddControlSegment_Cancel_m7C5D1FC9D38261A9E92740048C5DA77BC4787B5B,
	AddControlSegment_ActivationSwap_m6591E6144166C3622E742F37483B27DE75C27C7B,
	CameraController_Start_mE04AA0B2A1B81955FAAF5FD2A4DBBA22B61294FA,
	CameraController_Update_m7AC3B3CBCF845D071DAE7334957547734CCF9C82,
	CameraController_CheckObjectInfo_m72AAE3E912CB97CA2C97A1CCD470BDCEE0710541,
	CameraController__ctor_mC5858AFEA543DF84156A102F625A5774DBA7043E,
	MoveButton__ctor_m9CA6A1CFBB036D5C799D94A0945D8705445AFE9A,
	MoveButton_Awake_m6FFCDCB3F414E2F675DEF6029A64115A96B25066,
	MoveButton_OnDestroy_mEC61C3BFC7E776DEED876A6787B7E83C69B92937,
	MoveButton_OnShowPossibleTreeComplete_m3273A6C8DA1EC81BCC841E9507CF11E1CAC2EA4C,
	MoveButton_OnShowPossibleRockComplete_m1AD7E6E6BBB1C79A08CBF26A7191AF74F5F78B5A,
	MoveButton_Move_m44D188154B4446A04BB07227C1C52BC162FA32C1,
	ObjectPool__ctor_mABE989CA0A63A7924799A5B0E597CCC9E0F412F5,
	PossibleRock__ctor_m47F3F1870CEE7BCD52D8FFE3E79D3EE7E5B4560C,
	PossibleRock_Awake_m65CF3FFFD2F5FF27E74BF5367098162A4D42E7FC,
	PossibleRock_OnDestroy_m2383BBA41CBA7A2C060B2CD5BC0CD5171F062B04,
	PossibleRock_OnAddObjectComplete_m241307C9D8F12080A80F5A2277B7BB2549AEF17A,
	PossibleRock_OnAcceptPossibleObject_mE06ADDB3164DC66538E65F8C3FAAABA0090798B7,
	PossibleRock_OnCancelPossibleObject_mB31DE06C0DB1A7DD3AFA57C107CFF2CEAEADBF58,
	PossibleRock_DestroyRock_mF1E79A510E88E229BCD519BE32D8C9B1F9135D30,
	PossibleRock_OnMovePossibleObjectComplete_m75E92DA696F041DC49DB569A6238C4D88A5EF6EE,
	PossibleRock_OnShowPossibleRockStart_m3812B947411202F1422CDB8FD858E204EA0FE256,
	PossibleRock_PositionRock_mCB013D1BE03A110C166DE81CAE935C161B4ECBA4,
	PossibleRock_U3CPositionRockU3Eb__16_0_m01C22BB83342951BA5A2329666AB59346C2BE414,
	PossibleTree__ctor_m420FB70CA9B2A7E2D075F5E1137737E0FFE17DF0,
	PossibleTree_Awake_mF601F9F5004E0B9041880711358C23AB55B4BDA6,
	PossibleTree_OnDestroy_m9C25462AF736E27AC40B685C8CDC7ADB97E316EE,
	PossibleTree_OnAcceptPossibleObject_mF7296996385CC8BF25419DFBC4E0F31FF08AC1C0,
	PossibleTree_OnAddObjectComplete_mBA2A54D1187D9F1FFDF47ED2D8A24A58826C00AA,
	PossibleTree_DestroyTree_m3BC9F12E7F1560578D9AC8417473DC96878D2FC4,
	PossibleTree_OnCancelPossibleObject_m9BCA729794231453DFEF55432762E21FAB35C3D5,
	PossibleTree_OnMovePossibleObjectComplete_m309045EC5D1BDEE348F1C79BA0A5C1AEB2C24141,
	PossibleTree_OnShowPossibleTreeStart_m8CA98F71A9C01FC9D5ED4EFFD575A92C8E35D814,
	PossibleTree_PositionTree_m67F1EFF928AD79754014DD2F23F6788AA3453007,
	PossibleTree_U3CPositionTreeU3Eb__16_0_mB776B8B8860390BA15E01B559F0DF54894B02BE2,
	RockGenerator__ctor_m978A91D4EA3E32D440A98672A640DBA88C6134C7,
	RockGenerator_Awake_m53E7AA0224555A0841F6DCD9D84C10977712A372,
	RockGenerator_OnDestroy_m3B8389561A1C0514342CD0DCD15C798077B5F241,
	RockGenerator_OnGenerateRock_mBCD7EB89CBB178C5738E62F42D29F658207C5C43,
	RockGenerator_U3COnGenerateRockU3Eb__5_0_m5D761C18C6D34F9278E850AD7D6EE3EBE0C3CFF8,
	RockObjectPool__ctor_mC7C90EC2270E6115FEDEB60296DEE2A17FED8353,
	RockPreview_AddRock_m2B70EFF2658753AE2655435327FE3CF36303FE02,
	RockPreview__ctor_m56926744183297F943C655D9C98CA254E98D0CF8,
	RockView__ctor_m7F1D7A825523AB9892D65B6BC1C272811B46DBCD,
	RocksObjectPools_Awake_mF441324B6498DC15EDBE2CB30DEC5FDF718B73CE,
	RocksObjectPools_Start_m639B8F40D52F0606C9C2989EB14D552B30975028,
	RocksObjectPools_GetPooledObject_mF0FE6305A3F4CBD9E0BF7DA3BB4346831BD52E76,
	RocksObjectPools_PutBackPooledObject_m6A8D92AC4CF5CF78E1ED5E2109A357B937CF3255,
	RocksObjectPools__ctor_m77CD66E45F83B3BB6DC2DA170DCFAA372CE29B15,
	U3CU3Ec__DisplayClass4_0__ctor_m66706973119C8C8478FC96E1D11D7D529D276928,
	U3CU3Ec__DisplayClass4_0_U3CGetPooledObjectU3Eb__0_m6AA210BEEAB8FAF911B98D87759F26520312A730,
	TileOutline_ChangeVision_m6C358F9F3686C6E6EA1D72F4CD3F6E935BACB890,
	TileOutline__ctor_mE2A62F588012B218D33BBCC29D11ADBF697F00AE,
	TreeGenerator__ctor_m9BFB608E04BF29E8663D7BC9F01215132137F75A,
	TreeGenerator_Awake_m26AF1CEF65411DBB8DF605E6DB30319361DAB104,
	TreeGenerator_OnDestroy_m4BD50FCDF893AB107965BFA323D91685B04B0F14,
	TreeGenerator_OnGenerateTree_m4A989E8DD552360166730BFF7AD6601D74B03587,
	TreeGenerator_U3COnGenerateTreeU3Eb__5_0_m2FC9D272A4005F560A0129A93EAAD5C4B5EC5AD4,
	TreeObjectPool__ctor_m39CF883D1BAF86696B98E430491506EF16C17DE5,
	TreePreview_AddTree_m55464ACC0FBA33FFB1F5637DF7822C7578AB2D7A,
	TreePreview__ctor_m7496D05607C635A02CF0DD0A138A694C8400C3C7,
	TreeView__ctor_mDA69663D1436EE204210A26D082F0F6299CD59A9,
	TreesObjectPools_Awake_m1704B8F431BD888984674469C1E440CFDA630D06,
	TreesObjectPools_Start_m5C0648B4B7450CA1B8009BF26F0C420C7A7D42FE,
	TreesObjectPools_GetPooledObject_m703667CC836752AA4C39DA5AD3273359996D260D,
	TreesObjectPools_PutBackPooledObject_m8925FCD834954A1EBB75E7968F4D44742085C400,
	TreesObjectPools__ctor_m543A2FB862D8CDDDC50FA8FAF453392C546412DC,
	U3CU3Ec__DisplayClass4_0__ctor_m9CD88BA7E34F423CB602F2F7B53198EFCE638ED7,
	U3CU3Ec__DisplayClass4_0_U3CGetPooledObjectU3Eb__0_m1438ED2504569B257CC8D6EA434A47295F8D6217,
	GameData_FindGameObjectDataByUniqueID_mCFFC2D2B025ED5C864E45E6F23D3A147A5305787,
	GameData_FindOccupiedTilesByUniqueID_m364B2EA91A7F155688424388B5EE08336997BE26,
	GameData__ctor_m7633BB28CB93BBCAAE513D18E7D242DE61853BEB,
	GameData__cctor_m523205A9D014BB669C839EEBCEB3F7EFFB43CB2C,
	U3CU3Ec__DisplayClass10_0__ctor_m7B8547AB811C38DE96B61EA6A5E2CCB52E53AF78,
	U3CU3Ec__DisplayClass10_0_U3CFindGameObjectDataByUniqueIDU3Eb__0_mB12AA113EDD45B50B3535FD1064840C70BEFFC44,
	U3CU3Ec__cctor_m42DF686420142647887B5D78F67CEF80AD14DF46,
	U3CU3Ec__ctor_m9E3F20B7E5E75A981EBB6996638ABB039D2B3425,
	U3CU3Ec_U3CFindGameObjectDataByUniqueIDU3Eb__10_1_mFFE91556070B70D04097125B4AD5B5A8C85277AA,
	U3CU3Ec_U3CFindOccupiedTilesByUniqueIDU3Eb__11_1_m1B797AC68F76B6B56EFB1EC7CFD2C454387BE5B1,
	U3CU3Ec__DisplayClass11_0__ctor_m215085D56AB8D943DEE1502DF70B8E94DE61AD20,
	U3CU3Ec__DisplayClass11_0_U3CFindOccupiedTilesByUniqueIDU3Eb__0_m98B99F88F16DA7964C4A34B96D29BCCB64C35797,
	GameObjectData__ctor_mBF438C9887A5D44B7D5A4A1BC897265C9BDB8DA8,
	GameObjectData__ctor_m90F65DC8E9A35658BD2FCCEA7E488ACF5DCD7454,
	RockData__ctor_m0AF954A9DF11FED6D4E6574F3B0603656EEB7B05,
	RockData_GetSizeByType_mD97AC797676084C5348106C53E61A6E7E5B6082F,
	RockData_DefineOccupiedTiles_m40F51F8C8297FEBE93BF87002A8B425C3F2E31A6,
	TreeData__ctor_mAB813EFAFB5337DD07890173C52571DCE7820ED4,
	TreeData_GetSizeByType_m85CB220415FAFB9E0F9DD27C2536C72189DF30DF,
	TreeData_DefineOccupiedTiles_mE2E28371CF04B261B81690FC047F4B961F78D300,
	AddObjectComplete_OnExecute_m6A7D85CDEAE4E3042B3D658386D052D5A544F609,
	AddObjectComplete__ctor_mDD7F8445BDDEC0E773A904CB940BB7A74FA5E583,
	AddRock__ctor_m65CE6FF13D06481DC58813B0D0480BDCD2522052,
	AddRock_OnExecute_m59102DF176896057CDDD85F90A0F908DBF5A7CB9,
	AddRock_Activate_m506C81956266EB1798EE7A7C23C68452186FFFB3,
	AddRock_DeActivate_mDCF136E029275627EFB292610331BCBACC15B56A,
	AddTree__ctor_mD0BF05F992E8640FAC3CD04C47A5CEECE688BB57,
	AddTree_OnExecute_m6E79CC3BF20C2B4C1258E0E7FAECE2FF22EB4AA3,
	AddTree_Activate_mB3256B51C22CF73017695B771574A832C9E4C180,
	AddTree_DeActivate_m00B11E36A0E469DD647A660A4B3C0FD387986789,
	CheckPerimeterGenerated_OnExecute_m791F1DA6BB00B2D3B33F3E061C9C5C8EEDB7BB34,
	CheckPerimeterGenerated__ctor_m46D86C6F25004072B141E307F6EEB72F8B850095,
	U3CU3Ec__cctor_mFDA2DB212DE1EABDA6BC0CD09EF3E4004426BCF9,
	U3CU3Ec__ctor_m052986B92CE66527C0E56DC0AEBB7494A9B17D10,
	U3CU3Ec_U3COnExecuteU3Eb__0_0_mFA74F722FA08DD80ABE3C9E3861ACC79836E3CBB,
	DefineAvailableObjectSize_OnExecute_m5722452E5D793027F33AEE3F04A6EBD1C966446B,
	DefineAvailableObjectSize_Guard_m80A3B4323130E48A43CAB25CE37C249669FE62CA,
	DefineAvailableObjectSize__ctor_m6E2BAAA97C3A30BA4E11223025000C08E02341C7,
	DefineDirectionForGeneration_OnExecute_mEDC9F7D505566539A8DAB4BB133A1BD08DE19731,
	DefineDirectionForGeneration_Guard_m93B97140F77A8A89605EEDB63A91D7B65EAF62D9,
	DefineDirectionForGeneration__ctor_m846372B3FBA2260D4FECA38DF5057AD1B28DDE38,
	DefinePositionForGeneration_OnExecute_mEF3BD2A1A8C749CC64CE7B57D575244151B435BA,
	DefinePositionForGeneration_Guard_mA2C55CA03215E65C8333A1E25F014E8A81B74697,
	DefinePositionForGeneration__ctor_m906A1532A5850A99CBAEB4191839B7D74EE1B92E,
	GameLogic__ctor_mD2FA42BEC03973480C3DB1F9B6E99BA7BFBB6582,
	GameLogic_Awake_mC6C09DFA8CF50A93BF1DFBDDC2CD44741399D229,
	GameLogic_Start_m1410B88AC0E1B4FA97B517949EB18DFA753E7646,
	GameLogic_OnDestroy_m99D3791BD28BAFE931DB44641D267CACF152E28D,
	GameLogic_OnVerifyPossibleTree_mCCC8B3473732C384804647BE39C9064191F42498,
	GameLogic_OnVerifyPossibleRock_m8F08268DD574AA6175E385E77F1E7540218E5775,
	GameLogic_OnMovePossibleObject_mA7FDFE5AE3E4294FF51C083039E07D9DBF4F1509,
	GameLogic_OnShowPossibleTree_m001483863720C1E1EF929506795A347495495D97,
	GameLogic_OnShowPossibleRock_m942B19E7F5204DFC4489562C48341BD158D52718,
	GameLogic_OnDelayBetweenIterationComplete_mD6484B89E3C0C63B81663A6445372965C4BDDE1F,
	GameLogic_OnGenerationIterationComplete_m50C7EADE4CF7CAF8C9D8F5D5FD67DC07E6866755,
	GameLogic_OnGenerationIteration_mA706BB4AC4BE17A8B10AC5402FF58BD6C163BD60,
	U3CU3Ec__cctor_mE26C467EDC8085EC3BDE9DA0A5756EFCCE230690,
	U3CU3Ec__ctor_m50B0526E3AF8A9F012C0E22452BD21DA6B0527A2,
	U3CU3Ec_U3COnVerifyPossibleTreeU3Eb__19_0_m3F875215589D6D2D98AD06A8890CE2903F834D2D,
	U3CU3Ec_U3COnVerifyPossibleRockU3Eb__20_0_mF148E11EABF1780BE26BA6CB174E84D281ABBFA6,
	GenerateRandomObject__ctor_m6497E93DCE25FFE1B10E6107E8E49657B0164852,
	GenerateRandomObject_OnExecute_mEB3CFA342723E2825307341AC162D49F6DCE7FE5,
	GenerateRandomObject_Activate_m5449EE028447C17F6842CCF7C99D704001C43B3A,
	GenerateRandomObject_DeActivate_m60EC8624ECD99F354EE242ACEE673E480E3E04E2,
	GenerateRandomObject_Guard_m8D0FDC3B2F3A0163B3A29CFA6BD16FD4E849970B,
	U3CU3Ec__cctor_mB52F8E5D6C4CF3B9AEFABA1E5008D19A6724098A,
	U3CU3Ec__ctor_mC6778BFE0353F11ED559D8EE883900D5A2CFA3C0,
	U3CU3Ec_U3COnExecuteU3Eb__3_0_m6795201D994429F11655DEB8AF9832A59BBAD247,
	U3CU3Ec_U3COnExecuteU3Eb__3_1_m06AB8CE3FCD2C51D0147D118DB8982616C6E763D,
	GenerationIteration_OnExecute_mD6A31F5EAC8825464A5CE08E51E58BF76FACF948,
	GenerationIteration__ctor_mD5A18E4411D185941D262C42901878278F43C0B0,
	InitGameMapEmpty_OnExecute_m634F50767C4FD92241B07D8AF9B3D446B424761F,
	InitGameMapEmpty__ctor_mDBAA3F062F0919313D94721BEC28657C8212B31C,
	InitSizeGameObjectsRelation_OnExecute_m695B13CB0AC475478C707B8F8272E9DD94519AB2,
	InitSizeGameObjectsRelation__ctor_mB710872D30F71FF27D6C86D6B7A626A5DF311278,
	ShowPossibleRock__ctor_m82F637669DB376786C013DDF9956B2F3B01CB023,
	ShowPossibleRock_OnExecute_m85A22F32D3BE90DD45813DF3486F8FC8D31527C0,
	ShowPossibleRock_Activate_mCEB5126A50FE15B616F28BAFF8A8F233F85C9704,
	ShowPossibleRock_DeActivate_m645A007F853EBF894E7895382C7D7AF327AAD910,
	ShowPossibleTree__ctor_m6369E9CD36BFE8451795E86079E97E1534C136B1,
	ShowPossibleTree_OnExecute_mA2B9760896A52EDEF70F0F7C6403D93BDD8C1018,
	ShowPossibleTree_Activate_mDA3EC974C16A31E5F28FB03FC3DBE58CE89232E5,
	ShowPossibleTree_DeActivate_mC9788F8D306F91160B99F5AE2F90C1A8C72D56AA,
	RockTypeGuidDelegate__ctor_mBC9B3C1BA34DB43861192FB72917A12FE595C228,
	RockTypeGuidDelegate_Invoke_m4D7D6320C89BCD40815174E2CFD3E3948EDD6998,
	RockTypeGuidDelegate_BeginInvoke_mD27D1B88E5A6DEE55A25F9BE764E8FDA5B8F84A5,
	RockTypeGuidDelegate_EndInvoke_mD8CFE553B854ECF994082042E5B4C86B7A4D0A5F,
	TreeTypeGuidDelegate__ctor_mED9199B1A2E9ADA698C2A2FF27E52544E639D31F,
	TreeTypeGuidDelegate_Invoke_m0AF6FFB5FF658C612B4C6402D223A54D6668D9EF,
	TreeTypeGuidDelegate_BeginInvoke_mB4F19110770B02EB85ABEC7D8E5BFB8DDC6956B9,
	TreeTypeGuidDelegate_EndInvoke_mC4CA8A839494FA5E642D7BB769FEA640F6B5CAFD,
	RockTypeDelegate__ctor_m5553AE760EA76069028F66B4B25FCB965D78BBA5,
	RockTypeDelegate_Invoke_mE8A1950803C5A4617F699076CD65842901A40876,
	RockTypeDelegate_BeginInvoke_m756B8BFEC6CEC625186195454B9DF63234F5A9DC,
	RockTypeDelegate_EndInvoke_mCBF34F2D3B609DA3D9CC684907DAE458D6E858A3,
	TreeTypeDelegate__ctor_m4568BC654890C48EE908740F51BE990B8AC5DCA5,
	TreeTypeDelegate_Invoke_m484DB0A5FD8AB0A6F2335AF9BE0666FE0912CC43,
	TreeTypeDelegate_BeginInvoke_m207AEA6535C58034D6EEFE907CB93237720EA6AA,
	TreeTypeDelegate_EndInvoke_m149FB9958A6A890AFC9C6AAC8E32F1E813EF06AC,
	Vector3IntVector2IntDelegate__ctor_mC783EA6E7EF6F7B57648CF555E9FC67745226DC9,
	Vector3IntVector2IntDelegate_Invoke_mB6D04F9A660265DC656B712F576363175A9FA30E,
	Vector3IntVector2IntDelegate_BeginInvoke_m9BC8B1BFBEF78257E5DBC7AA0FB0E312CF65BFAD,
	Vector3IntVector2IntDelegate_EndInvoke_m196891133FD447634111054EA8821E972DCB2568,
	RockViewDelegate__ctor_mCAD28039F45E633B125C2B02345E3976BAEA7245,
	RockViewDelegate_Invoke_m2ADEFED25721C6694839442B4C48BD65D8E8A6C0,
	RockViewDelegate_BeginInvoke_m99D9AE8F006DCBEAEFBF6EDDD9E8568ABEDAEB68,
	RockViewDelegate_EndInvoke_mD2E1DD3BCCAB3D2A2D10FE11D3EF61C26121220B,
	TreeViewDelegate__ctor_m2DF3D3E414E6994894FDFBE746AEEB2698F33E4E,
	TreeViewDelegate_Invoke_m381A96A4F32924955CBA228D6EBDFB28DBC10218,
	TreeViewDelegate_BeginInvoke_mEFA3FB3C2640E5F893F85B267581F38ECE04792C,
	TreeViewDelegate_EndInvoke_mD689120A5845DA0591A3C0ED1F6520AA3FE0ED00,
	GameCameraConfig__cctor_m705146193CC5B80951E6E6F2FC3E87DB6C22183B,
	GameMapConfig__cctor_mB798A1C5B908BDC306B0621DD0B2A5128114A9C4,
	SizeConfig__cctor_mD0DB491428EF534782F0CE1BF301DB07E86F6B5F,
	ObjectExtensions_IsPrimitive_mE3413D220E53574C90F68C8C789487FB8E1340F3,
	ObjectExtensions_Copy_m49548073C536D88E334ED506ECED6833B5F42EF5,
	ObjectExtensions_InternalCopy_m231ED0B451B4803AA35877ECA937C5ABE7896E6E,
	ObjectExtensions_RecursiveCopyBaseTypePrivateFields_m01D153B957C26F29A83EC530F783CABC08F69637,
	ObjectExtensions_CopyFields_m8AEBB8646E9DC2AB83B37AB277E9CFC4A35C408E,
	NULL,
	ObjectExtensions__cctor_m7D2B24D9EE3450F3F6DB06204F47EF593D96D3C8,
	U3CU3Ec__DisplayClass3_0__ctor_mD5542E6AB3FA5C38F2C8AE42F559A9AE81A3C48F,
	U3CU3Ec__DisplayClass3_1__ctor_mEA88BBA45FC2CBA1BC27CDF235B3E9E7E418A5EF,
	U3CU3Ec__DisplayClass3_1_U3CInternalCopyU3Eb__0_mFFAF34C84F89FFD93615734E815C13AA2B771D82,
	U3CU3Ec__cctor_mC45DE533C026BAC3A6A04C8336D94660821ECB5A,
	U3CU3Ec__ctor_m6D2B9138E5D9D034277A71C8A73271186005E11F,
	U3CU3Ec_U3CRecursiveCopyBaseTypePrivateFieldsU3Eb__4_0_mB047F81F9F692FA41781AE64EBFAEFF05A006609,
	ReferenceEqualityComparer_Equals_m830A6744426166FEC917035E62E2B8911BFD84B5,
	ReferenceEqualityComparer_GetHashCode_mD52702F2722A2955E1FBDAEFB0B1F556B9C176EC,
	ReferenceEqualityComparer__ctor_m1B21137BC03F0048695FF25B4D6D2CF34ED495AA,
	ArrayExtensions_ForEach_m28922D9F29A0093D1EE37A70AEF1AC1F485E59BA,
	ArrayTraverse__ctor_mCFDE8139EB5CD94042B84E151802FAF8D3D682A3,
	ArrayTraverse_Step_m56CF3C645443EDB9AC909E45F3792B1B0B11475D,
	CanvasAutoResize_OnEnable_m3AD0334FDD4331B71E250BA51553CD3A4A9A5019,
	CanvasAutoResize__ctor_mDD19FB2A76E4077833CB0AC6F775517D23207528,
	FadeScreenView__ctor_m2D606BDC2D4E1760BEC48194C12E32191E08D1E8,
	FadeScreenView_Awake_mECF628CDF7464AFDCAF18E86D9DDA11E721B728C,
	FadeScreenView_OnDestroy_m564CCFBD770C638C40BA9E8F9F9E46A3852403CF,
	FadeScreenView_ClearTween_m96E2FA183C419BA33055C2FDEEF2C1CC4C9299C6,
	FadeScreenView_OnFadeScreen_m25D5FD52A7FB2B89A00F71189F0469B6E435D170,
	FadeScreenView_OnFadeOutScreen_m516CCD0E883397721E5C7AF3EE47036E042A8550,
	FadeScreenView_U3COnFadeOutScreenU3Eb__10_0_m5C0EF6DF8886C3BC32FBFFDD97F3D718A17727A6,
	U3CU3Ec__cctor_mA76D2A86A164B763014083751F3256F0F36DFF5A,
	U3CU3Ec__ctor_m130045401B8D8A1538092FDE11E669791D237A3C,
	U3CU3Ec_U3COnFadeScreenU3Eb__9_0_m56FE9BCF514C4C5BE73179484C588EDC4E38851A,
	SafeArea_Awake_m5F9C4BAB44820BA7C2CEE88799894AA9676B4031,
	SafeArea__ctor_m7685F4CA5E5037198D49E9AF2FEA5F6F8AF809E7,
	UserInterfaceLocker__ctor_m7DB40D8EC1151A866D059177550C007CBECB92B2,
	UserInterfaceLocker_OnEnable_mABF1ACFCDB8ABEEAC3C46E4E61FE63EE0A80035F,
	UserInterfaceLocker_OnDisable_m2260099229194452EB16E5CBCC8C9A700C76AF53,
	UserInterfaceLocker_OnUnLock_m781B9DB71241A1E48EBC375CF5E83BA0E189A984,
	UserInterfaceLocker_OnLock_mF8CDFD4131D843C6063000FD6A8C1A06391C64EC,
	BaseTask_get_inProgress_mDA8BF0F90B97AC0165A19FF0EAC9412D28C06D5B,
	BaseTask_set_inProgress_m8A9CB52395583D9689BE39C7B51E5AD67F5AD36D,
	BaseTask_ForceModeOn_mC79E2EAA70E4183832344451A814D82A43792C2E,
	BaseTask_Activate_mF1A5137EAAB72F844BB6558D02DB473C4BE63678,
	BaseTask_DeActivate_m491554BBCC754389E193EEA6086989EC4B0FEF4D,
	BaseTask_ForceComplete_m01D5FEB2BA5BB63066FFC10F88C48410EA8A8AEF,
	BaseTask_Execute_mC071A3C21E2FD567CF9A2602B3C70CE9517203DC,
	BaseTask_Guard_m0BA8470E3BB673D9F51CCC2A115C6D3D1D512DDE,
	BaseTask_OnGuard_mD7C45815D9BDF88CC6454D0333742B9D8A3F57F0,
	BaseTask_Complete_m9FF8D6F02082567CF9D6D36B15D8067D44C1F015,
	BaseTask_Terminate_m90A563088C350AD2E0A5BDA273F05B2CC1968BD2,
	BaseTask_OnTerminate_m7E816CF9836573AEA460781F23C85EE740D10AA9,
	BaseTask_OnExecute_m2EE62F18BA83F88CD06D0087BD4229628EC127B4,
	BaseTask_OnForceExecute_m71559EF7834F7B8DF388A92086DBBE84882E43EE,
	BaseTask__ctor_mCB916DF503E63F75CB330C095E34ABE4D48C7532,
	DelayTask_OnExecute_mA7397B8AAF3752E96B02108266DA02F4B55396D7,
	DelayTask_DelayComplete_m18CF5B94D88F6DB5F1353255CC03FE4AAE195F05,
	DelayTask_Complete_m39FFEBB63A9801BE7563CA78F269D2114FDC63BE,
	DelayTask_OnTerminate_m1BA233109CAA3321E1224C3B2868C5E088E399FC,
	DelayTask_ClearDelay_mB4C9F85F357C14BFD583993D14E5B84B78645682,
	DelayTask_OnDestroy_m95ADEA2CE295986A8BA16730ABFD6D75663FDC52,
	DelayTask__ctor_m56543B6730DBB9113E4733E1F8782CF7259C09B1,
	LoopSequence_Complete_m95E0767EC773F310ADF59E0451E4F7C7AC90B298,
	LoopSequence__ctor_m74ABFDDB0EC942A495E5A41959A7C7CB7D8DE672,
	ParallelTask_Awake_m21CFF8CD3713DD7B79FAE9977DB13ADF05FAA350,
	ParallelTask_DeActivate_m49C2DB3625F90638991A369DC45ACAFF0B3F91EA,
	ParallelTask_OnChildTaskComplete_m8C6821FE302FC94E9B12F42F78E4292951FE5560,
	ParallelTask_OnChildTaskGuard_m93853BA5B44B13F75E826561BF6A1684419D7407,
	ParallelTask_OnForceExecute_m8F9D57B84ABD1554BF29162A93507EB8FA1185C0,
	ParallelTask_OnExecute_mBCE45C10F3F215352C0A22B7B0C238D292C37CB3,
	ParallelTask_ForceModeOn_mB17B343A45DD7C387170C294A75CEF0ED2BF7FEA,
	ParallelTask_OnTerminate_m399A03BEF1E2FCBCF7DF2CC63A319BA13F208A49,
	ParallelTask__ctor_mC6AA50D04C1ED2DF093C43829C5074709C8B79C8,
	SequenceTask_Awake_mB106FB2C0B971B313A8578F7AB42DFDC59C1D48C,
	SequenceTask_DeActivate_mDFE9F383BD44B8AD8ADC5932B61E29EEDC6808FC,
	SequenceTask_OnChildTaskComplete_m726189F465C21A6198F7BBD6D95B0DC518137982,
	SequenceTask_OnChildTaskGuard_m048D7C2EC39559FB946303E9961B087888471B30,
	SequenceTask_OnForceExecute_mFACE297E774DE005634AC9A1958CDBB25F0D5E28,
	SequenceTask_OnExecute_m0CFA3437DF12189626CA3374A140CEFB7F7A0EDE,
	SequenceTask_NextTask_mED2A3E582A9E0EE0C7219A28EB155DA66E936D80,
	SequenceTask_ForceModeOn_m28F8CAC1271503FD6D7C2DD09AF54926ED40C3FC,
	SequenceTask_OnTerminate_m3A45A40EA912222BE7EE43D5F3B852F7278B28FA,
	SequenceTask__ctor_m900ABCEDE774E883351847CAE97E58E87071F070,
	MainModel_get_Instance_m9CA908B2B90D7AA5EFE64F9D138D2E05A03B98F4,
	MainModel_get_GameData_mDEDAECA9AF12EF27CFF9962E582161880E1D6A5F,
	MainModel_set_GameData_mA943CF7D6AEE4B0D5684106FD4C90E3356B852FF,
	MainModel_IsNull_m84315D41DC57A8FF87A5C713F428062DCB2DB301,
	MainModel_Init_m33292F75E1F5BF26B12CA915955FABD7E666DD22,
	MainModel__ctor_m0D67886949C03C3CC8642AC492A50C39A72BFA3B,
	Observer_get_instance_m7C1960F33B8EDD3279901CEF27B68360CB549701,
	Observer_Init_m703BA2F07719623E769FD7F66006561B7DBBF974,
	Observer_IsNull_m2AB010437EBBFC983B0DBD0E5923B887CD8DCB8C,
	Observer_AddListener_m42738DFF2FF70F0F4B02E92EF67CC35957F6D09F,
	Observer_RemoveListener_m949C560C91E701AD2DD5FFF2D085C25D790BFE40,
	Observer_Emit_m81B123C91DF6729DD7B546693B57E344FB5E99F2,
	Observer__ctor_mDAA046FC2B9FDA6036DF5DA00BFFCCC9A4435CBD,
	FadeOutScreen__ctor_mAB57C61FF756BC83007E28465556A64FAC6211D3,
	FadeOutScreen_OnExecute_m8A35E67ADF0D1584A7715698F72A400ED9B464AC,
	FadeOutScreen_Activate_m0E301803199084BE7B3F2A2AA06BDF7E8518A8FC,
	FadeOutScreen_DeActivate_m62F4A8251606434A7D90BF73E157BD9367A7A11D,
	FadeScreen__ctor_m1D87124A13454BEC77B14C1657BE22C535661C0D,
	FadeScreen_OnExecute_mCFFA10DD2336A4AD451F4B76DE013E8C8FAF8EF8,
	FadeScreen_Activate_mA200730C878D687126DBD45497734ADB0A2E4F4D,
	FadeScreen_DeActivate_mB9CEAFCC75E3F226EEDAD238A6DE30F4B2F50ECB,
	LockUserInterface_OnExecute_m15F3318F745C13204B5116C25098F907C6419B0C,
	LockUserInterface__ctor_m6A53A6A2DE617ECB1BBFB57C683FDB4F37AB50F4,
	UnLockUserInterface_OnExecute_m6BDC0A0DC9B2C929B2B4AA8294999E27E0AB4777,
	UnLockUserInterface__ctor_m41DE93D23E509A4AAD57BCC19F24675E1BFF3342,
	SimpleDelegate__ctor_m2E868FB5069AFD65CE57DBC517B2FDCA8590B0F1,
	SimpleDelegate_Invoke_m614A63B4251A1A0C7937E13BFD96E726783B8D4F,
	SimpleDelegate_BeginInvoke_m9D726B2DE38240F3503F74F753F6FAA85724B766,
	SimpleDelegate_EndInvoke_mB3D5A911A068606E57755482A66B517A276CC11C,
	ReturnIntSimpleDelegate__ctor_mD21E3CC47176D55226CF839C9AFA10E4C0C0A6E3,
	ReturnIntSimpleDelegate_Invoke_m4341C9FCA402B25C3FAF0F684F12A1C621D1C072,
	ReturnIntSimpleDelegate_BeginInvoke_m75871ED5CDC17D2F8760DA20FF50C7F646EC27B3,
	ReturnIntSimpleDelegate_EndInvoke_mA7C2B72F905287F2AB680C7E8F50631400734440,
	TaskDelegate__ctor_mB888EF198554486F35A5E40772A6A849B4502228,
	TaskDelegate_Invoke_m2342944B9977B07715EF94A0BFA34CBAE7B02AA0,
	TaskDelegate_BeginInvoke_m4D8804ABA093F7E0DE92132428B8616930E295A8,
	TaskDelegate_EndInvoke_m7C789AE7A416592A29D3684D393912261892A264,
	BoolDelegate__ctor_mB9310402E1B4D3F8379ACEE4185C4BE7BE0EE692,
	BoolDelegate_Invoke_mC52BA7DD5BC6EBBA7F5AC9B5FB0EC353DFF66C6F,
	BoolDelegate_BeginInvoke_mBFAD186494E93C8F42546F0E688160E06D085C78,
	BoolDelegate_EndInvoke_mE72D712D3DF42FDA97808F8D044FF02A74C3B083,
	IntDelegate__ctor_m56FD9C29DB5F90C4C4790B073796BD8C2FD1292A,
	IntDelegate_Invoke_mDA997F958C361CCD1C74C09DF153D00D0D5F1CBA,
	IntDelegate_BeginInvoke_mABD76ADAAF7545F0C086E07068DB0446B1315808,
	IntDelegate_EndInvoke_m963FC51DDD5522A7F1168574E1D4C9D2E7889CB5,
	IntIntDelegate__ctor_m46CA6ECA7E1BC80FE1394C10DA409423CEE59CCF,
	IntIntDelegate_Invoke_m52F7E8C6246FF3ACFB0948F152C3DF7E6BC79576,
	IntIntDelegate_BeginInvoke_mFA6A62C5FEC4A86F4EE8C6EA33579343977529A0,
	IntIntDelegate_EndInvoke_m43B20B298828F13553BD7E07D09B09599C3CE880,
	FloatDelegate__ctor_m08E8FE9B6517FC2A72F691B1F534D6A431948376,
	FloatDelegate_Invoke_mBC917CFC7D1E123C8FDE74CB13680BF285C3A4FD,
	FloatDelegate_BeginInvoke_mB1288165236956665F157980EE2C5A127E5EAF9F,
	FloatDelegate_EndInvoke_mB2B7E192660642B03453C786385FED321AA21C5A,
	StringDelegate__ctor_m908786AB82F31EA91BD74E529D74F4E0D0568F10,
	StringDelegate_Invoke_m3F5D3C51C57214F9BE2B6F25B997CFBFC5D822A2,
	StringDelegate_BeginInvoke_mC545894BE023A46DD56D8A525ECFD13E69CD5929,
	StringDelegate_EndInvoke_mAD4E31A175D8141C29EA5CA5345C1FF2DD41CD1F,
	Vector2IntDelegate__ctor_m6FFDA70806B0CA3ABE899C9E76FEE91E91602FD9,
	Vector2IntDelegate_Invoke_mBFD7148B6FE83FFB6AE2AD9B5BE39CEC7F547E74,
	Vector2IntDelegate_BeginInvoke_m9F5E25F263EC0DACB5A465CC5899412003EE9E40,
	Vector2IntDelegate_EndInvoke_mD90467D5F822A6DE7A814B23B52470BE1D55369B,
	Vector3Delegate__ctor_m73A537DCF00AC53073D1711EF3A8CFD31165E17C,
	Vector3Delegate_Invoke_mBEC64925D910E779C406887207F444B1B53AD249,
	Vector3Delegate_BeginInvoke_m2F3B39BC71CDAAA15F1A6FC50DD173719FF93092,
	Vector3Delegate_EndInvoke_mE006EE61AAB89B029080E24C6672637F8E12D7E6,
};
static const int32_t s_InvokerIndices[344] = 
{
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1228,
	1228,
	1417,
	1417,
	1417,
	1218,
	1218,
	1417,
	1417,
	1417,
	1417,
	1417,
	1256,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1168,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1168,
	1417,
	1417,
	1417,
	778,
	1168,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1000,
	2455,
	1417,
	1417,
	1098,
	1417,
	1417,
	1417,
	1417,
	1417,
	778,
	1168,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1000,
	2455,
	1417,
	1417,
	1098,
	998,
	998,
	1417,
	2496,
	1417,
	1039,
	2496,
	1417,
	994,
	1169,
	1417,
	1039,
	1417,
	1218,
	1218,
	2440,
	2389,
	1218,
	2440,
	2389,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	2496,
	1417,
	1098,
	1417,
	1404,
	1417,
	1417,
	1404,
	1417,
	1417,
	1404,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	859,
	1218,
	1218,
	1228,
	1228,
	1417,
	2496,
	1417,
	1098,
	1098,
	1417,
	1417,
	1417,
	1417,
	1404,
	2496,
	1417,
	1098,
	1098,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	832,
	778,
	216,
	1228,
	832,
	778,
	216,
	1228,
	832,
	1218,
	390,
	1228,
	832,
	1218,
	390,
	1228,
	832,
	859,
	250,
	1228,
	832,
	1228,
	405,
	1228,
	832,
	1228,
	405,
	1228,
	2496,
	2496,
	2496,
	2415,
	2392,
	2159,
	1858,
	1551,
	-1,
	2496,
	1417,
	1417,
	833,
	2496,
	1417,
	1098,
	688,
	953,
	1417,
	2285,
	1228,
	1404,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	2496,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1404,
	1244,
	1417,
	1417,
	1417,
	1417,
	1417,
	1404,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1228,
	1228,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1228,
	1228,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	2480,
	2480,
	2455,
	2488,
	1417,
	1417,
	2480,
	1417,
	2488,
	2285,
	2285,
	2285,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	1417,
	832,
	1417,
	635,
	1228,
	832,
	1374,
	635,
	953,
	832,
	1228,
	405,
	1228,
	832,
	1244,
	424,
	1228,
	832,
	1218,
	390,
	1228,
	832,
	780,
	217,
	1228,
	832,
	1246,
	426,
	1228,
	832,
	1228,
	405,
	1228,
	832,
	1255,
	439,
	1228,
	832,
	1256,
	440,
	1228,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x060000CC, { 0, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)2, 143 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	344,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
